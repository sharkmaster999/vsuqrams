<?php



error_reporting(E_ALL);
set_include_path(get_include_path() . PATH_SEPARATOR . '../../phpexcel/Classes/');

include 'PHPExcel/IOFactory.php';
include "../../process/CollegeDAO.php";

$event_id = $_POST["event_id"];
$excel_file = $_FILES["excel_file"];
$process = new CollegeDAO();

$target_dir = "../../files/";
$target_file = $target_dir . basename($excel_file["name"]);
$temp = explode(".", $excel_file["name"]);
$newfilename = $temp[0] . "-" . date("Ymdhis") . ".".$temp[1];

if(!move_uploaded_file($excel_file["tmp_name"], $target_dir . $newfilename)) {
    echo "Failed to upload";
} else {
    $inputFileName = $target_dir . $newfilename;
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);


    foreach($sheetData as $row) {
        $process->addTimeLogData($row["A"], $event_id, $row["B"], date("Y-m-d H:i:s"));
    }

    header("Location: ../../college/operate_event.php?event_id=". $event_id. "&success=2");
}


