<?php

session_start();

include "../../process/CollegeDAO.php";

$process = new CollegeDAO();
$id = $_POST["act_curr_id"];
$actsem = $process->getActiveSemester($id);

$_SESSION["year"] = $actsem["year"];
$_SESSION["semester"] = $actsem["semester"];


$sem = "";
if($_SESSION["semester"] == 1) {
    $sem = "1st Semester";
} elseif ($_SESSION["semester"] == 2) {
    $sem = "2nd Semester";
} elseif ($_SESSION["semester"] == 3) {
    $sem = "Summer";
}

$_SESSION["success"] = "School year <b>". $_SESSION["year"] . "-". ($_SESSION["year"]+1). " ". $sem. "</b> has now <b>active</b> in this session.";

header("Location: ../../college/index.php");