<?php

include "../../process/CollegeDAO.php";

session_start();

$auth_type = $_POST["auth_type"];
$type = $_POST["type"];
$student_id = $_POST["student_id"];
$year = $_SESSION["year"];
$sem = $_SESSION["semester"];
$fk_id = $_SESSION["fk_id"];

$process = new CollegeDAO();

$process->payAllPaymentByType($student_id, $auth_type, $type, $year, $sem, $fk_id);

echo "OK";
