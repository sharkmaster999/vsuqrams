<?php

include "../../process/CollegeDAO.php";

$name = $_POST["name"];
$dept_acro = $_POST["dept_acro"];
$course_name = $_POST["course_name"];
$course_acro = $_POST["course_acro"];
$course_major = $_POST["course_major"];
$college_id = $_POST["college_id"];

if($course_major == "") {
	$course_major = " ";
}

$process = new CollegeDAO();

$process->addDepartment($name, $course_name, $dept_acro, $course_acro, $course_major, $college_id);

header("Location: ../../college/department.php?success=2");