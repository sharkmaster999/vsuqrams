<?php

include "../../process/CollegeDAO.php";

$name = $_POST["name"];
$date = $_POST["date"];
$type = $_POST["type"];
$fk_id = $_POST["fk_id"];
$fines = $_POST["fines"];
$year = $_POST["year"];
$semester = $_POST["semester"];
$auth_id = $_POST["auth_id"];
$sign_ctr = $_POST["sign_ctr"];

$process = new CollegeDAO();

$process->addEvent($name, $year, $semester, $fines, $auth_id, $date, $type, $fk_id, $sign_ctr);

header("Location: ../../college/college_info.php?id=". $fk_id. "&success=1");

?>