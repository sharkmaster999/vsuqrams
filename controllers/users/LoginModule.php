<?php


include "../../process/UserDAO.php";

session_start();

$student_id = $_POST["student_id"];
$password = $_POST["password"];

$process = new UserDAO();

$data = $process->loginUser($student_id, $password);
$actsem = $process->getActiveSemester();

if($data != null) {
    if($data["type"] == "University") {
        $_SESSION["student_id"] = $student_id;
        $_SESSION["password"] = $password;
        $_SESSION["type"] = $data["type"];
        $_SESSION["fk_id"] = $data["fk_id"];
        $_SESSION["year"] = $actsem["year"];
        $_SESSION["semester"] = $actsem["semester"];
        $_SESSION["auth_type"] = $data["status"];
        header("Location: ../../administrator/");
    } elseif($data["type"] == "College") {
        $_SESSION["student_id"] = $student_id;
        $_SESSION["password"] = $password;
        $_SESSION["type"] = $data["type"];
        $_SESSION["fk_id"] = $data["fk_id"];
        $_SESSION["year"] = $actsem["year"];
        $_SESSION["semester"] = $actsem["semester"];
        $_SESSION["auth_type"] = $data["status"];
        header("Location: ../../college/");
    }
} else {
    session_destroy();
    header("Location: ../../?err=1");
}