<?php 

include "../../process/AdministratorDAO.php";

session_start();

$process = new AdministratorDAO();

$org_type = $_POST["org_type"];
$student_id = $_POST["stud_id"];
$password = $_POST["password"];

if($org_type == "University") {
	$u_role = $_POST["u_role"];
	$process->addAccount($student_id, $password, $org_type, 0, $u_role);
	$_SESSION["success"] = "User account successfully added in university authorization.";
	header("Location: ../../administrator/user_management.php");
} elseif ($org_type == "College") {
	$c_id = $_POST["c_assign"];
	$c_role = $_POST["c_role"];
	$process->addAccount($student_id, $password, $org_type, $c_id, $c_role);
	$_SESSION["success"] = "User account successfully added in college authorization.";
	header("Location: ../../administrator/user_management.php");
} elseif($org_type == "Department") {
	$d_id = $_POST["d_assign"];
	$d_role = $_POST["d_role"];
	$process->addAccount($student_id, $password, $org_type, $d_id, $d_role);
	$_SESSION["success"] = "User account successfully added in department authorization.";
	header("Location: ../../administrator/user_management.php");
} else {
	header("Location: ../../administrator/user_management.php");
}
?>