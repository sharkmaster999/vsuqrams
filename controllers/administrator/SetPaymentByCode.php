<?php

include "../../process/AdministratorDAO.php";

$stud_num = $_POST["query"];
$payment_id = $_POST["payment_id"];
$type = $_POST["payment_type"];
$total_pay = $_POST["total_pay"];

$process = new AdministratorDAO();


$ctr = $process->checkIfExistPayment($stud_num, $payment_id, $type);
if($ctr == 0) {
    $result = $process->getDataPaymentByCode($stud_num, $payment_id, $type, $total_pay);
    $data = json_encode($result);

    echo $data;
} else {
    $arr[] = null;
    $arr["p_id"] = 0;
    echo json_encode($arr);
}


?>