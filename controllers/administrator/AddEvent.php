<?php

include "../../process/AdministratorDAO.php";

$name = $_POST["name"];
$date = $_POST["date"];
$type = $_POST["type"];
$fk_id = $_POST["fk_id"];
$fines = $_POST["fines"];
$year = $_POST["year"];
$semester = $_POST["semester"];
$auth_id = $_POST["auth_id"];

$process = new AdministratorDAO();

$process->addEvent($name, $year, $semester, $fines, $auth_id, $date, $type, $fk_id);

header("Location: ../../administrator/college_info.php?id=". $fk_id. "&success=1");

?>