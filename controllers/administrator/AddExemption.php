<?php


include "../../process/AdministratorDAO.php";

$stud_ids = $_POST["noqr_stud"];
$event_id = $_POST["event_id"];
$exempt = $_POST["exemption"];
$process = new AdministratorDAO();

$data = $process->addExemptionByStudentID($stud_ids);

foreach($data as $row1) {
    foreach ($row1 as $row) {
        if($exempt == "Exempted") {
            $process->timeLog($row["student_id"], "Exempted", $event_id);
        } elseif($exempt == "No QR IN") {
            $process->timeLog($row["student_id"], "No QR IN", $event_id);
        } elseif($exempt == "No QR OUT") {
            $process->timeLog($row["student_id"], "No QR OUT", $event_id);
        }
    }
}

header("Location: ../../administrator/operate_event.php?event_id=". $event_id. "&success=exemption");