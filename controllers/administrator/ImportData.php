<?php

error_reporting(E_ALL);
set_include_path(get_include_path() . PATH_SEPARATOR . '../../phpexcel/Classes/');

include 'PHPExcel/IOFactory.php';
include "../../process/ServerDAO.php";

$excel_file = $_FILES["excel_file"];
$file_type = $_POST["file_type"];
$process = new ServerDAO();

$target_dir = "../../files/";
$target_file = $target_dir . basename($excel_file["name"]);
$temp = explode(".", $excel_file["name"]);
echo $temp[0] . "-" . date("Ymdhis") . ".".$temp[1];
$newfilename = $temp[0] . "-" . date("Ymdhis") . ".".$temp[1];

if(!move_uploaded_file($excel_file["tmp_name"], $target_dir . $newfilename)) {
    echo "Failed to upload";
} else {
    $inputFileName = $target_dir . $newfilename;
    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    if($file_type == "Students") {
        foreach($sheetData as $row) {
            //$fullname = explode(", ", $row["B"]);
            //echo $fullname[1]."";
			// Add data guide: student_number, $lastname, $firstname, $middlename, $course, $major, $college, $level

            // For major if null
            if($row["G"] == "" || $row["G"] == null)
                $row["G"] = " ";

            //echo $row["A"], " ",  $row["E"], " ",  $row["F"], " ", $row["G"], " ", $row["B"], " ", $row["C"], " ", $row["H"], " ", $row["D"], " ", " <br>";
            $process->addData($row["A"], $row["B"], $row["C"], $row["D"], $row["E"], $row["G"], $row["H"], $row["F"]);

        }

        header("Location: ../../administrator/data_management.php?success=1");
    } else {
        foreach($sheetData as $row) {
            $process->addTimeLogData($row["A"], $row["B"], $row["C"], date("Y-m-d H:i:s"), strtotime($row["D"]));
        }

        header("Location: ../../administrator/data_management.php?success=2");
    }

}


