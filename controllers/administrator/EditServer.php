<?php

include "../../process/ServerDAO.php";

$id = $_POST["server_id"];
$host = $_POST["host"];
$username = $_POST["username"];
$password = $_POST["password"];

$process = new ServerDAO();

$process->editServer($id, $host, $username, $password);

header("Location: ../../administrator/data_management.php?success=2");
