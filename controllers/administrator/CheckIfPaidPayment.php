<?php

include "../../process/AdministratorDAO.php";

$stud_num = $_POST["student_id"];
$type = $_POST["type"];
$payment_id = $_POST["payment_id"];

$process = new AdministratorDAO();

$ctr = $process->checkIfExistPayment($stud_num, $payment_id, $type);

if($ctr != 0) {
    $id = $process->getPaymentLogID($stud_num, $type, $payment_id);

    echo $id;
} else {
    echo $ctr;
}


?>