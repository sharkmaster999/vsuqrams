<?php


include "../../process/AdministratorDAO.php";

session_start();

$id = $_GET["id"];
$year = $_GET["year"];
$semester = $_GET["semester"];

$process = new AdministratorDAO();

$process->setAsActiveSemester($id);

$sem = "";
if($semester == 1) {
    $sem = "1st Semester";
} elseif ($semester == 2) {
    $sem = "2nd Semester";
} elseif ($semester == 3) {
    $sem = "Summer";
}

$_SESSION["success"] = "School year <b>". $year . "-". ($year+1). " ". $sem. "</b> has now active by <b>default</b>";

header("Location: ../../administrator/settings.php");

