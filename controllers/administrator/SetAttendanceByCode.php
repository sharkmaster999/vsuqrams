<?php

include "../../process/AdministratorDAO.php";

$stud_num = $_POST["query"];
$status = $_POST["status"];
$event_id = $_POST["event_id"];

$process = new AdministratorDAO();

$ctr1 = $process->counterData($stud_num);

if($ctr1 != 0) {
    $ctr = $process->checkIfExistTime($stud_num, $status, $event_id);

    $result = $process->getDataByCode($stud_num, $status, $event_id, $ctr);
    $data = json_encode($result);
    echo $data;
}


?>