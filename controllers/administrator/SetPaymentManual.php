<?php

include "../../process/AdministratorDAO.php";

$stud_num = $_POST["student_id"];
$or_number = $_POST["or_number"];
$total_pay = $_POST["total_pay"];
$amt_pay = $_POST["amt_pay"];
$amt_tender = $_POST["amt_tender"];
$type = $_POST["o_type"];
$payment_id = $_POST["payment_id"];

$process = new AdministratorDAO();

$ctr = $process->checkIfExistPayment($stud_num, $payment_id, $type);

if($ctr == 0) {
    $id = $process->addPaymentLog($stud_num, $or_number, $total_pay, $amt_pay, $amt_tender, $type, $payment_id);

    echo $id;
} else {
    $id = $process->getPaymentLogID($stud_num, $type, $payment_id);

    echo $id;
}


?>