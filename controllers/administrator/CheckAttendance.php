<?php

include "../../process/AdministratorDAO.php";

$stud_num = $_POST["student_id"];
$log_type = $_POST["log_type"];
$event_id = $_POST["event_id"];

$process = new AdministratorDAO();

$res = $process->checkTimeLogJSON($stud_num, $log_type, $event_id);

if($res != "") {
	echo date("h:i:s A", strtotime($res));
} else {
	echo "";
}


?>