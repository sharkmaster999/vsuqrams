<?php


include "../../process/AdministratorDAO.php";

session_start();

$year = $_POST["year"];
$semester = $_POST["semester"];

$process = new AdministratorDAO();

$process->addSemester($year, $semester);

$sem = "";
if($semester == 1) {
    $sem = "1st Semester";
} elseif ($semester == 2) {
    $sem = "2nd Semester";
} elseif ($semester == 3) {
    $sem = "Summer";
}

$_SESSION["success"] = "School year ". $year . "-". ($year+1). " ". $sem. " has been added successfully.";

header("Location: ../../administrator/settings.php");

