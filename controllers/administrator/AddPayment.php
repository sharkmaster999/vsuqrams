<?php


session_start();

include "../../process/AdministratorDAO.php";

$process = new AdministratorDAO();

$name = $_POST["name"];
$description = $_POST["description"];
$amount = $_POST["amount"];
$year = $_POST["year"];
$semester = $_POST["semester"];
$auth_id = $_POST["auth_id"];
$type = $_POST["type"];
$fk_id = $_POST["fk_id"];

$process->addPayment($name, $description, $amount, $year, $semester, $auth_id, $type, $fk_id);

$_SESSION["success"] = "You successfully add <b>new payment</b> for this school period.";

header("Location: ../../administrator/payment.php");