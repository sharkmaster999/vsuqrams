<?php

include "../../process/AdministratorDAO.php";

$lastname = $_POST["lastname"];
$firstname = $_POST["firstname"];
/*$middlename = $_POST["middlename"];*/
$middlename = "";
$student_id = $_POST["student_id"];
$course = $_POST["course"];
$college = $_POST["college"];
$major = $_POST["major"];

$process = new AdministratorDAO();

$process->addStudent($student_id, $lastname, $firstname, $middlename, $course, $major, $college, 'default.png');

header("Location: ../../administrator/student.php?success=1");

?>