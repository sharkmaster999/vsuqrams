<?php
include "../process/CollegeDAO.php";

session_start();

if($_SESSION["student_id"] == null && $_SESSION["password"] == null && $_SESSION["type"] != "College") {
    session_destroy();
    header("Location: ../?err=session_expired");
}

$process = new CollegeDAO();
$college_data = $process->getCollegeData($_SESSION["fk_id"]);

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Visayas State University QR Code Attendance Monitoring System | Administrator</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-5">
            <h3>Student Record Tracker</h3>
            <?php include "active_sem.php";?>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <br>
        </div>
        <div class="col-md-7">
            <div style="float: right; margin-top: 3px;">
                <br>
                <div class="form-group">
                    <select class="form-control" id="camera-select" style=""></select>
                    <!--<button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>-->
                    <!-- <button title="Image shoot" class="btn btn-info btn-xs disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span> Capture</button> -->
                    <a href="#addStudent" class="btn btn-primary btn-xs" data-toggle="modal"><span class="glyphicon glyphicon-plus"></span> Register Student</a>
                    <a href="generate_qr.php?college=<?php echo $college_data["acronym"];?>" class="btn btn-primary btn-xs" data-toggle="modal"><span class="glyphicon glyphicon-qrcode"></span> Generate QR Code</a>
                    <button title="Play" class="btn btn-success btn-xs" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> Start</button>
                    <button title="Pause" class="btn btn-warning btn-xs" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> Pause</button>
                    <button title="Stop streams" class="btn btn-danger btn-xs" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span> Stop</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="well" style="padding: 3px;">
                        <canvas width="113" height="113" id="webcodecam-canvas"></canvas>
                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="well" style="padding: 3px;">
                        <img width="113" height="90" id="scanned-img" src="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="data-wrapper" style="width: 100%">
                <div class="image-storage" style="float: left;">

                </div>
                <div id="studentData">
                    <div style="border: 5px dashed #cccccc; text-align: center; padding: 20px; height: 200px;">
                        <br><br><br>
                        <h4 style="vertical-align: middle; color: #cccccc">Student information will display here if the system scan completely and has a data.</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <form action="generate_qr_by_student.php" method="POST">
                        <input type="hidden" name="noqr_stud" class="form-control noqr_stud"/>
                        <button class="btn btn-primary btn-xs generate_qr" style="float: right"><span class="glyphicon glyphicon-qrcode"></span> Generate</button>
                    </form>
                    <br><br>
                    <table class="table table-bordered student-list">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Student ID</th>
                            <th>Names</th>
                            <th>Course</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $college_data = $process->getCollegeData($_SESSION["fk_id"]);
                            $process->viewCollegeStudents($college_data["acronym"]);
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <h3 class="modal-title" id="myModalLabel">Register Student</h3>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/AddStudent.php" method="POST" class="student-frm">
                        <p >Please fill-up all the fields to register.</p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Student ID: </label>
                                    <input type="text" name="student_id" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Last Name: </label>
                                    <input type="text" name="lastname" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">First Name and M.I.: </label>
                                    <input type="text" name="firstname" class="form-control" autocomplete="off" placeholder="(e.g.: Allan T)"/>
                                </div>
                            </div>
                            <!--<div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Middle Name: </label>
                                    <input type="text" name="middlename" class="form-control" autocomplete="off"/>
                                </div>
                            </div>-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">College: </label>
                                    <select name="college" id="" class="form-control">
                                        <option>--- Select College ---</option>
                                        <option value="CoE">CoE</option>
                                        <option value="CFES">CFES</option>
                                        <option value="CAFS">CAFS</option>
                                        <option value="CAS">CAS</option>
                                        <option value="CME">CME</option>
                                        <option value="CVM">CVM</option>
                                        <option value="CE">CE</option>
                                        <option value="CN">CN</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="#">Course Acro: </label>
                                <input type="text" name="course" class="form-control" autocomplete="off"/>
                            </div>
                            <div class="col-md-4">
                                <label for="#">Major: </label>
                                <input type="text" name="major" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary add-student-btn"><span class="glyphicon glyphicon-user"></span> Add Student</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="generateQR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Generate QR Code By College</h3>
                </div>
                <div class="modal-body">
                    <form action="generate_qr.php" method="POST" class="generateqr-frm">
                        <p >Please choose what college you want to generate code</p>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="#">College: </label>
                                    <select name="college" id="" class="form-control">
                                        <option>--- Select College ---</option>
                                        <option value="CoE">College of Engineering</option>
                                        <option value="CFES">College of Forestry and Environmental Science</option>
                                        <option value="CAFS">College of Agriculture and Food Science</option>
                                        <option value="CAS">College of Arts and Sciences</option>
                                        <option value="CME">College of Management and Economics</option>
                                        <option value="CVM">College of Veterinary Medicine</option>
                                        <option value="CE">College of Education</option>
                                        <option value="CN">College of Nursing</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary generateqr-btn"><span class="glyphicon glyphicon-user"></span> Generate</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                        $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-plus"></span> Add Semester</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                            $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/qrcodelib.js"></script>
    <script type="text/javascript" src="../js/webcodecamjquery.js"></script>
    <script type="text/javascript" src="../js/mainjquery.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>
    <script type="text/javascript" src="../js/administrator.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>