<?php
/**/

$host = "localhost";
$username = "root";
$password = "";
$db_name = "vsuqrams";
$dbh = null;

$xls_filename = $_GET["college"]. ' - '. $_GET["event_name"].' - EXPORTED '.date('Y-m-d').'.xls'; // Define Excel (.xls) file name

$dbh = new PDO("mysql:host=". $host. ";dbname=".$db_name, $username, $password);

if($_GET["college"] == "ALL") {
    $sql = "SELECT stud_num, id, lastname, firstname, course FROM tbl_user ORDER BY course, id, lastname";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
} else {
    $sql = "SELECT stud_num, id, lastname, firstname, course FROM tbl_user WHERE college = ? ORDER BY course, id, lastname";
    $stmt = $dbh->prepare($sql);
    $stmt->bindParam(1, $_GET["college"]);
    $stmt->execute();
}

// Header info settings
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$xls_filename");
header("Pragma: no-cache");
header("Expires: 0");
header("Charset: UTF-8");
/***** Start of Formatting for Excel *****/
// Define separator (defines columns in excel &amp; tabs in word)
$sep = "\t"; // tabbed character

echo "Student ID". $sep. "Last Name". $sep. "First Name". $sep. "Course". $sep. "Time In". $sep. "Time Out". $sep. "\n";

// Start while loop to get data
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $schema_insert = "";
    $schema_insert .= $row["stud_num"].$sep;
    $schema_insert .= utf8_decode($row["lastname"]).$sep;
    $schema_insert .= utf8_decode($row["firstname"]).$sep;
    $schema_insert .= $row["course"].$sep;

    // Time in

    $sql1 = "SELECT * FROM tbl_time_log WHERE student_id = ? AND event_id = ? AND status = 'Time-in' ORDER BY id DESC LIMIT 1";

    $stmt1 = $dbh->prepare($sql1);
    $stmt1->bindParam(1, $row["stud_num"]);
    $stmt1->bindParam(2, $_GET["event_id"]);
    $stmt1->execute();
    $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);

    if(!(isset($row1["status"]))) {
        $schema_insert .= "".$sep;
    } else {
        $schema_insert .= "1".$sep;
    }

    // Time out

    $sql1 = "SELECT * FROM tbl_time_log WHERE student_id = ? AND event_id = ? AND status = 'Time-out' ORDER BY id DESC LIMIT 1";

    $stmt1 = $dbh->prepare($sql1);
    $stmt1->bindParam(1, $row["stud_num"]);
    $stmt1->bindParam(2, $_GET["event_id"]);
    $stmt1->execute();
    $row1 = $stmt1->fetch(PDO::FETCH_ASSOC);

    if(!(isset($row1["status"]))) {
        $schema_insert .= "".$sep;
    } else {
        $schema_insert .= "1".$sep;
    }

    

    $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(trim($schema_insert));
    print "\n";
}
?>