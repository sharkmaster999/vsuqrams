<?php

$event_id = $_POST["event_id"];

$host = "localhost";
$username = "root";
$password = "";
$db_name = "vsuqrams";
$dbh = null;

$xls_filename = 'export_time_log_'.date('Y-m-d').'.xls'; // Define Excel (.xls) file name

$dbh = new PDO("mysql:host=". $host. ";dbname=".$db_name, $username, $password);

$sql = "SELECT * FROM tbl_time_log WHERE event_id = ?";
$stmt = $dbh->prepare($sql);
$stmt->bindParam(1, $event_id);
$stmt->execute();

// Header info settings
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$xls_filename");
header("Pragma: no-cache");
header("Expires: 0");
header("Charset: UTF-8");

/***** Start of Formatting for Excel *****/
// Define separator (defines columns in excel &amp; tabs in word)
$sep = "\t"; // tabbed character

// Start while loop to get data
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $schema_insert = "";
    $schema_insert .= $row["student_id"].$sep;
    $schema_insert .= $event_id.$sep;
    $schema_insert .= $row["status"].$sep;
    $schema_insert .= $row["time_enter"].$sep;

    $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(trim($schema_insert));
    print "\n";
}