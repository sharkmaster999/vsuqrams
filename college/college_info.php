<?php include "header.php";?>
<?php include "navigation.php";?>
<?php
$college_data = $process->getCollegeData($_SESSION["fk_id"]);
?>


<div class="container">
    <h3><?php echo ucwords($college_data["name"]) ." (" .strtoupper($college_data["acronym"]) . ")";?></h3>
    <?php include "active_sem.php";?>
    <?php 
        if($_SESSION["auth_type"] == 0) {
            echo '<a href="#addEvent" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Create Event</a>';
        }
    ?>
    <hr>
    <div class="">
        <h4>Major Events</h4>
        
        <div class="row">
            <?php
                $process->viewCollegeEvent($_SESSION["fk_id"], $_SESSION["year"], $_SESSION["semester"]);
            ?>
        </div>
    </div>
</div>
<div class="modal fade" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Create New Event</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/college/AddEvent.php" method="POST" class="event-frm" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name: </label>
                                <input type="hidden" id="type" name="type" value="College"/>
                                <input type="hidden" id="fk_id" name="fk_id" value="<?php echo $_SESSION["fk_id"]?>"/>
                                <input type="hidden" name="year" value="<?php echo $_SESSION["year"]?>"/>
                                <input type="hidden" name="semester" value="<?php echo $_SESSION["semester"]?>"/>
                                <input type="text" id="name" name="name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date">Date: </label>
                                <input type="text" id="date" name="date" class="form-control event-date" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="fines">Fines: </label>
                                <input type="text" id="fines" name="fines" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="auth_stud_id">Authorized Student ID #: </label>
                                <input type="text" id="auth_stud_id" name="auth_id" class="form-control" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="sign_ctr">Sign Req.: </label>
                                <select class="form-control" name="sign_ctr">
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm add-event-btn"><i class="fa fa-plus"></i> Create</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php";?>
