<?php
session_start();

if($_SESSION["student_id"] == null && $_SESSION["password"] == null && $_SESSION["type"] != "College") {
    session_destroy();
    header("Location: ../?err=session_expired");
}
?>
<?php

include "../process/CollegeDAO.php";

$process = new CollegeDAO();

$data = $process->getEventData($_GET["event_id"]);
$college_data = $process->getCollegeData($data["fk_id"]);
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Visayas State University QR Code Attendance Monitoring System | College</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-12 attendance-notif">

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <h3><?php echo ucwords($college_data["name"])?></h3>
            <h4><?php echo ucwords($data["name"])?></h4>
            <h5>Event Date: <b><?php echo date('l, F d, Y', strtotime($data["date"]))?></b></h5>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <input type="hidden" id="event_id" class="event_id_fld" name="event_id" value="<?php echo $_GET["event_id"];?>"/>
        </div>
        <div class="col-md-7">
            <br>
            <div style="float: right;">
                <select class="form-control" id="camera-select" style="float: right;"></select>
				<br><br>
                <div style="float: right; margin-right: 0;">
                    <button class="btn btn-primary btn-xs trigger_log" style="" value="Time-in"><i class="fa fa-clock-o"></i> Time-in</button>
                    <button class="btn btn-primary btn-xs trigger_log" value="Time-out"><i class="fa fa-clock-o"></i> Time-out</button>

                    <!--<button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                    <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>-->
                    <button title="Play" class="btn btn-primary btn-xs" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> Start</button>
                    <button title="Pause" class="btn btn-warning btn-xs" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> Pause</button>
                    <button title="Stop streams" class="btn btn-danger btn-xs" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span> Stop</button>
                    <?php
                        if($college_data["acronym"] == "ALL") {

                            echo '<button class="btn btn-success btn-xs" href="#exportByCollege" type="button" data-toggle="modal"><span class="glyphicon glyphicon-export"></span> Export by College</button>';
                        }

                        if($_SESSION["auth_type"] == 0) {
                            echo '<a href="excel_reader.php?event_id='. $_GET["event_id"]. '&event_name='. ucwords($data["name"]).'&college='. $college_data["acronym"]. '" class="btn btn-primary btn-xs" style="margin-right: -3px;"><span class="glyphicon glyphicon-export"></span> Export Logs</a>&nbsp;&nbsp;';
                            echo '<a href="#importData" data-toggle="modal" class="btn btn-primary btn-xs" style="margin-right: -3px;"><span class="glyphicon glyphicon-import"></span> Import Logs</a>';
                        }
                    ?>
					<br>
                    <div class="active-status-note">Active Status: <span class="active_log" style="font-weight: bold; color: #008020">Time-in</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="well" style="padding: 3px;">
                        <canvas width="113" height="113" id="webcodecam-canvas"></canvas>
                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="well" style="padding: 3px;">
                        <img width="113" height="90" id="scanned-img" src="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="data-wrapper" style="width: 100%">
                <div class="image-storage" style="float: left;">

                </div>
                <div id="studentData">
                    <div style="border: 5px dashed #cccccc; text-align: center; padding: 20px; height: 160px;">
                        <br><br>
                        <h4 style="vertical-align: middle; color: #cccccc">Student information will display here if the system scan completely and has a data.</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                
                <div class="col-md-12"> <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#logs" data-toggle="tab"><i class="fa fa-clock-o"></i>Time Log</a></li>
                        <li><a href="#exempt" data-toggle="tab"><i class="fa fa-users"></i> Exemption</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="logs">
                            <br>
                            <table id="timeLog" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Student ID</th>
                                    <th>Name</th>
                                    <th>Course</th>
                                    <th>Status</th>
                                    <th>Log Time</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="log_data">
                                <?php
                                $process->getTimeLogData($_GET["event_id"]);
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="exempt">
                            <br>
                            <table class="table table-bordered student-list table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Student ID</th>
                                    <th>Names</th>
                                    <th>Course</th>
                                    <!--<th style="width: 50px;">Time-in</th>
                                    <th style="width: 50px;">Time-out</th>
                                    <th style="width: 50px;">Exempted</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                
                                   <?php
                                        $process->viewStudentList($_GET["event_id"], $college_data["acronym"]);
                                   ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="modal" id="addExemption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b style="font-size: 12px;" class="modal-title ex-title" id="myModalLabel"></b>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" style="font-size: 12px;">
                        <tr>
                            <th>Time-in</th>
                            <th>Time-out</th>
                        </tr>
                        <tr>
                            <td class="disp-t1"><button class="btn btn-primary btn-xs">Time in now</button></td>
                            <td class="disp-t2"><button class="btn btn-primary btn-xs">Time out now</button></td>
                        </tr>
                    </table>
                </div>
                <!--<div class="modal-footer">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/college/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                            $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Import Data</h4>
                </div>
                <div class="modal-body">
                    <form action="../controllers/college/ImportLogByEventID.php" method="POST" class="import-frm" enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">File to Import: </label>
                                    <input type="hidden" name="event_id" value="<?php echo $_GET["event_id"]?>"/>
                                    <input type="file" name="excel_file" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary import-btn"><span class="glyphicon glyphicon-floppy-save"></span> Import</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exportByCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Export Data By College</h4>
                </div>
                <div class="modal-body">
                    <form action="excel_reader.php" method="GET" class="export-data-frm">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="hidden" name="event_id" value="<?php echo $_GET["event_id"]?>"/>
                                    <input type="hidden" name="event_name" value="<?php echo ucwords($data["name"])?>"/>
                                    <label for="#">College: </label>
                                    <select name="college" id="" class="form-control">
                                        <option>--- Select College ---</option>
                                        <option value="CoE">College of Engineering</option>
                                        <option value="CFES">College of Forestry and Environmental Science</option>
                                        <option value="CAFS">College of Agriculture and Food Science</option>
                                        <option value="CAS">College of Arts and Sciences</option>
                                        <option value="CME">College of Management and Economics</option>
                                        <option value="CVM">College of Veterinary Medicine</option>
                                        <option value="CE">College of Education</option>
                                        <option value="CN">College of Nursing</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary export-data-btn"><span class="glyphicon glyphicon-floppy-save"></span> Export Data</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/qrcodelib.js"></script>
    <script type="text/javascript" src="../js/webcodecamjquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>
    <script type="text/javascript" src="../js/event.js"></script>
    <script type="text/javascript" src="../js/attendance.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>