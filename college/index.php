<?php include "header.php";?>
<?php include "navigation.php";

$college_data = $process->getCollegeData($_SESSION["fk_id"]);
?>


<div class="container">
    <?php
    if(isset($_SESSION["success"])) {
        echo "<br><div class='alert alert-success'>". $_SESSION['success']."</div>";
        unset($_SESSION["success"]);
    }
    ?>
    <br>
    <div class="jumbotron">

        <?php include "active_sem.php";?>
        <div class="">
            <span style="margin-bottom: 5px; display: block">Powered by:</span>

            <img src="../img/vsu-logo.png" alt="" width="50" style="margin-right: 5px;"/>
            <img src="../img/ussc-logo.png" alt="" width="50" style="margin-right: 5px;"/>
            <img src="../img/cs_logo.png" alt="" width="50"/>
        </div>
        <h2>
            Welcome to <?php echo $college_data["name"];?>!<br>This is a project of the University Supreme Student Council, <br>the Quick Response Student Attendance Monitoring System
        </h2>
        <br>
        <p>This project is intended for all Viscan students to monitor attendance log for general assemblies, meetings and application for computing student fines and organizational fees.</p>

    </div>
</div>


<?php include "footer.php";?>
