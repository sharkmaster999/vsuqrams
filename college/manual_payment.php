<?php
include "../process/CollegeDAO.php";
session_start();
$process = new CollegeDAO();

$college_data = $process->getCollegeData($_SESSION["fk_id"]);
$data = $process->getPaymentData($_GET["payment_id"]);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | Administrator</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-4">
            <h3><?php echo ucwords($data["name"])?></h3>
            <h4>Amount: <?php echo $data["amount"]?></h4>
            <?php include "active_sem.php"?>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-bordered student-list table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Student ID</th>
                            <th>Names</th>
                            <th>Course</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $process->viewAllStudentListForPayment($college_data["acronym"]);
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal" id="addPaymentLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b style="font-size: 12px;" class="modal-title pay-title" id="myModalLabel"></b>
                </div>
                <div class="modal-body">
                    <form action="#" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <b>Amount to be pay: <br/> P <span style="font-size: 25px; color: #006e1c"><?php echo $data["amount"]?></span></b>
                                <input type="hidden" id="student_id" name="student_id" value=""/>
                                <input type="hidden" id="o_type" name="o_type" value="College"/>
                                <input type="hidden" id="payment_id" name="payment_id" value="<?php echo $_GET["payment_id"]?>"/>
                                <input type="hidden" id="total_pay" name="total_pay" value="<?php echo $data["amount"]?>"/>
                                <input type="hidden" id="amt_tender" name="amt_tender" value=""/>
                            </div>
                            <div class="col-md-6">
                                <b>Amount tendered: <br/> P <span id="amt-tnd-cls" style="font-size: 25px; color: #006e1c">0.00</span></b>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="#">O.R. Number: </label>
                                    <input type="text" id="or_number" name="or_number" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="#">Amount Pay:</label>
                                    <input type="text" id="amount_pay" name="amount_pay" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <span class="print-pay"></span>
                    <button class="btn btn-success btn-sm save-payment-btn"><i class="fa fa-save"></i> Save</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="alreadyPaid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b style="font-size: 12px;" class="modal-title pay-title1" id="myModalLabel"></b>
                </div>
                <div class="modal-body">
                    <b>This account has already paid this payment!</b>
                </div>
                <div class="modal-footer">
                    <span class="print-pay1"></span>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>