<?php

$sem = "";
if($_SESSION["semester"] == 1) {
    $sem = "1st Semester";
} elseif ($_SESSION["semester"] == 2) {
    $sem = "2nd Semester";
} elseif ($_SESSION["semester"] == 3) {
    $sem = "Summer";
}

?>
<div style="margin-bottom: 5px;"><b>Active School Year: <?php echo $_SESSION["year"]?>-<?php echo $_SESSION["year"]+1?> <?php echo $sem?></b></div>