<?php

session_start();
include "../process/CollegeDAO.php";

$process = new CollegeDAO();

$stud_data = $process->getInfoByStudentID($_GET["query"]);
$college_data = $process->getCollegeData($_SESSION["fk_id"]);

?>
<h3><?php echo $stud_data["lastname"];?>, <?php echo $stud_data["firstname"]. " ". $stud_data["middlename"];?></h3>
<span><b><?php echo $college_data["name"];?> - <?php echo $stud_data["course"];?></b></span>
<hr/>
<div>
    <!--<div style="background-color: #ffe601; border: 1px solid #005216; padding: 3px;"><b>University</b></div>
    <table class="table table-bordered collection-list">
        <thead>
        <tr>
            <th>Payment for:</th>
            <th>Amount</th>
            <th>Payment Made</th>
            <th>Amount to Pay</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <tbody>
        </tbody>
    </table>-->

    <div style="background-color: #ffe601; border: 1px solid #005216; padding: 3px;"><b>Obligations</b></div>
    <table class="table table-bordered collection-list">
        <thead>
        <tr>
            <th>University Payments:</th>
            <th>Amount</th>
            <th>Payment Made</th>
            <th>Amount to Pay</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $process->getUniversityPaymentByStudent($stud_data["student_id"], $_SESSION["year"], $_SESSION["semester"]);
        ?>
        </tbody>

        <thead>
        <tr>
            <th>College Events:</th>
            <th>Time-in</th>
            <th>Time-out</th>
            <th>Fines</th>
            <th>Paid Fines</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $process->getPaymentByStudent($stud_data["student_id"], $_SESSION["year"], $_SESSION["semester"], $_SESSION["type"], $_SESSION["fk_id"]);
        ?>
        </tbody>

        <thead>
        <tr>
            <th>College Payments:</th>
            <th>Amount</th>
            <th>Payment Made</th>
            <th>Amount to Pay</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $process->getCollegePaymentByStudent($stud_data["student_id"], $_SESSION["year"], $_SESSION["semester"], $_SESSION["fk_id"]);
        ?>
        </tbody>
    </table>
</div>