<?php
session_start();

if($_SESSION["student_id"] == null && $_SESSION["password"] == null && $_SESSION["type"] != "College") {
    session_destroy();
    header("Location: ../?err=session_expired");
}
?>
<?php

include "../process/CollegeDAO.php";

$process = new CollegeDAO();

$data = $process->getPaymentData($_GET["payment_id"]);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | College</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>

<?php include "navigation.php";?>

<div class="container" id="QR-Code">

    <div class="row">
        <div class="col-md-5">
            <h2><?php echo ucwords($data["name"])?></h2>
            <b>Amount to be pay: <?php echo $data["amount"]?></b>
            <?php include "active_sem.php"?>
            <input type="hidden" id="payment_id" name="payment_id" value="<?php echo $_GET["payment_id"]?>"/>
            <input type="hidden" id="total_pay" name="total_pay" value="<?php echo $data["amount"]?>"/>
            <input type="hidden" id="payment_type" name="payment_type" value="College"/>
        </div>

        <div class="col-md-7">

            <div class="attendance-notif" style="right: 104px; position: fixed; width: 20%; display: none;">
                <div class="alert alert-danger text-center"><b>The students are already recorded in the list!</b></div>
            </div>

            <div class="attendance-notif1" style="right: 104px; position: fixed; width: 20%; display: none;">
                <div class="alert alert-success text-center"><b>Successfully added!</b></div>
            </div>
            <br>
            <div style="float: right;">
                <select class="form-control" id="camera-select" style="float: right;"></select>
                <br><br>
                <div style="float: right; margin-right: 0;">
                    <!--<button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                    <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>-->
                    <button title="Play" class="btn btn-primary btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> Start Scan</button>
                    <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> Pause Scan</button>
                    <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span> Stop Scan</button>

                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-9">
            <div class="data-wrapper" style="width: 100%">
                <div class="image-storage" style="float: left;">

                </div>
                <div id="studentData">
                    <div style="border: 5px dashed #cccccc; text-align: center; padding: 20px; height: 200px;">
                        <br><br><br>
                        <h4 style="vertical-align: middle; color: #cccccc">Student information will display here if the system scan completely and has a data.</h4>
                    </div>
                </div>
            </div>
            <br>
            <h4>Time Log</h4>
            <table id="paymentLog" class="table table-bordered">
                <thead>
                <tr>
                    <th>Student ID</th>
                    <th>Name</th>
                    <th>Course</th>
                    <th>Status</th>
                    <th>Payment Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="log_data">
                <?php
                $process->getPaymentLogByPaymentID($_GET["payment_id"], "College");
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            <div class="well" style="">
                <canvas width="200" height="180" id="webcodecam-canvas"></canvas>
                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
            </div>
            <div class="well">
                <img width="225" height="180" id="scanned-img" src="">
            </div>
        </div>
    </div>

    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                        $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/qrcodelib.js"></script>
    <script type="text/javascript" src="../js/webcodecamjquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../js/event.js"></script>
    <script type="text/javascript" src="../js/payment-college.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
    <script type="text/javascript">
        $(function () {
            $(document).on("click", ".remove-pay-log", function () {
                var id = $(this).data("id");
                $.ajax({
                    url: "../controllers/administrator/DeletePaymentManual.php",
                    type: "POST",
                    dataType: "html",
                    data: {payment_id : id},
                    success: function(e) {
                        $(".payment-"+id).remove();
                    }
                });
            });
        })
    </script>
</body>
</html>