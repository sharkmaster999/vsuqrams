<?php include "header.php";?>
<?php include "navigation.php";?>
<?php

?>


<div class="container">
    <?php
        if(isset($_SESSION["success"])) {
            echo '<br><div class="alert alert-success">'. $_SESSION["success"]. '</div>';
            unset($_SESSION["success"]);
        }
    ?>
    <h3><i class="fa fa-users"></i> User Management <a href="#addUserRole" data-toggle="modal" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add User Role</a></h3>
    <hr/>
    <table class="table table-bordered student-list table-hover">
        <thead>
        <tr>
            <th>Student ID</th>
            <th>Accountable Student</th>
            <th>Organization</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            <?php
                $process->getUserAccounts($_SESSION["fk_id"]);
            ?>
        </tbody>
    </table>
</div>
<div class="modal fade" id="addUserRole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Add New User</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/college/AddAccount.php" method="POST" class="user-frm">
                    <h4>Basic Information</h4>
                    <hr/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#">Student ID: </label>
                                <input type="text" name="stud_id" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#">Password: </label>
                                <input type="password" name="password" class="form-control" autocomplete="off"/>
                                <input type="hidden" name="c_assign" value="<?php echo $_SESSION["fk_id"];?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#">Organization Type: </label>
                                <select name="org_type" id="#" class="form-control org_type">
                                    <option value="">--- Select One ---</option>
                                    <option value="College">College</option>
                                    <option value="Department">Department</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="ur_c_opt" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="#">Role: </label>
                                    <select name="c_role" id="#" class="form-control">
                                        <option value="0">Administrator</option>
                                        <option value="1">Encoder</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="ur_d_opt" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="#">Department Assigned: </label>
                                    <select name="d_assign" id="#" class="form-control d_assign">
                                        <?php 
                                            $process->viewDepartmentSelection($_SESSION["fk_id"]);
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="#">Role: </label>
                                    <select name="d_role" id="#" class="form-control">
                                        <option value="0">Administrator</option>
                                        <option value="1">Encoder</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-user-btn"><span class="fa fa-user"></span> Add User Role</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Set Semester</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/college/ActiveSemester.php" method="POST" class="semester1-frm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="#">School Year: </label>
                                <select name="act_curr_id" id="" class="form-control">
                                    <?php
                                        $process->getSchoolYearOption();
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
    $(function () {
        $(".org_type").on("change", function () {
            var o_type = $(this).val();

            if(o_type == "College") {
                $(".ur_d_opt").hide();
                $(".ur_c_opt").show();
            } else if(o_type == "Department") {
                $(".ur_c_opt").hide();
                $(".ur_d_opt").show();
            } else {
                $(".ur_c_opt").hide();
                $(".ur_d_opt").hide();
            }
        });

        $(document).on("click", ".add-user-btn", function () {
            $(".user-frm").submit();
        });
    
        $(".add-semester1-btn").on("click", function () {
            $(".semester1-frm").submit();
        });
    });
</script>

</body>
</html>
