
<?php
session_start();

if($_SESSION["student_id"] == null && $_SESSION["password"] == null && $_SESSION["type"] != "College") {
    session_destroy();
    header("Location: ../?err=session_expired");
}
?>
<?php
include "../process/CollegeDAO.php";

$process = new CollegeDAO();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | College</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "nav-fluid.php";

$college_data = $process->getCollegeData($_SESSION["fk_id"]);
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <h3>Collection</h3>
            <?php include "active_sem.php";?>
            <hr/>
            <table class="table table-bordered student-list">
                <thead>
                <tr>
                    <th></th>
                    <th>Student ID</th>
                    <th>Names</th>
                    <th>Course</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $process->viewStudentListForCollection(1, $college_data["acronym"]);
                ?>

                </tbody>
            </table>
        </div>
        <div class="col-md-7" style="border-left: 1px solid #ccc; min-height: 550px;">
            <div id="collection-list"></div>
        </div>
    </div>
</div>



<div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Set Semester</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/college/ActiveSemester.php" method="POST" class="semester1-frm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="#">School Year: </label>
                                <select name="act_curr_id" id="" class="form-control">
                                    <?php
                                    $process->getSchoolYearOption();
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../js/filereader.js"></script>
<!-- Using jquery version: -->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="../js/source.js"></script>

<!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
<script type="text/javascript" src="js/webcodecamjs.js"></script>
<script type="text/javascript" src="js/main.js"></script> -->
<script type="text/javascript">
    $(function () {
        $(document).on("click", ".collect-stud", function () {
            var student_id = $(this).data("id");
            searchStudent(student_id);
        });

        $(document).on("click", ".collect-payment", function () {
            $(this).addClass('disabled');
            var type = $(this).data("kind");
            var auth_type = $(this).data("org");
            var student_id = $(this).data("studid");

            $.ajax({
                type: "POST",
                url: "../controllers/college/PayAllPaymentByType.php",
                data: {auth_type: auth_type, type: type, student_id: student_id},
                dataType: "html",
                success: function (e) {
                    if(e == "OK") {
                        $("#tp_univ").html("P 0.00");
                        $("#tp_univ_btn").html("");
                    }
                }
            });
        });

        $(document).on("click", ".collect-fines", function () {
            $(this).addClass('disabled');
            var type = $(this).data("kind");
            var auth_type = $(this).data("org");
            var student_id = $(this).data("studid");

            $.ajax({
                type: "POST",
                url: "../controllers/college/PayAllPaymentByType.php",
                data: {auth_type: auth_type, type: type, student_id: student_id},
                dataType: "html",
                success: function (e) {
                    if(e == "OK") {
                        alert("Fines successfully paid!");
                        $("#tp_fines").html("P 0.00");
                        $("#tp_fines_btn").html("");
                    }
                }
            });
        });

        $(document).on("click", ".collect-payment-col", function () {
            $(this).addClass('disabled');
            var type = $(this).data("kind");
            var auth_type = $(this).data("org");
            var student_id = $(this).data("studid");

            $.ajax({
                type: "POST",
                url: "../controllers/college/PayAllPaymentByType.php",
                data: {auth_type: auth_type, type: type, student_id: student_id},
                dataType: "html",
                success: function (e) {
                    if(e == "OK") {
                        $("#tp_col").html("P 0.00");
                        $("#tp_col_btn").html("");
                    }
                }
            });
        });
    });

    function searchStudent(student_id) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("collection-list").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open ("GET","collection_info.php?query="+student_id+"", true);
        xmlhttp.send();
    }
</script>
</body>
</html>
