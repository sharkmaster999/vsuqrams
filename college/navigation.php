<nav class="navbar navbar-default" style="font-weight: bold;">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="index.php" style="margin-top: -6px; line-height: 0.8">
                <img src="../img/vsu-logo.png" alt="" width="45" style="margin-top: -7px; margin-right: 5px; float: left;"/>
                Visayas State University <br>
                <span style="font-size: 10px;">Quick Response Student <br> Attendance Monitoring System</span>
            </a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                <?php 
                    if($_SESSION["auth_type"] == 0) {
                        echo '<li><a href="students.php"><i class="fa fa-users"></i> Students</a></li>';
                    }
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-check"></i> Transactions <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="college_info.php">Attendance</a></li>
                        <li><a href="payment.php">Payment</a></li>
                        <li><a href="collection.php">Collection</a></li>
                        <!--<li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>-->
                    </ul>
                </li>
                <?php
                    if($_SESSION["auth_type"] == 0) {
                        echo '  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Settings <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="settings.php">General</a></li>
                                        <li><a href="department.php">Departments</a></li>
                                        <li><a href="user_management.php">User Management</a></li>
                                        <li><a href="data_management.php">Data Management</a></li>
                                        <li><a href="#activeSemester" data-toggle="modal">Set Semester</a></li>
                                        <!--<li role="separator" class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>-->
                                    </ul>
                                </li>';
                    }
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Hello <?php echo $process->getAccountLastName($_SESSION["student_id"])?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../controllers/users/LogoutModule.php"><i class="fa fa-logout"></i>Log-out</a></li>
                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>