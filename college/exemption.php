
<?php
session_start();

if($_SESSION["student_id"] == null && $_SESSION["password"] == null && $_SESSION["type"] != "College") {
    session_destroy();
    header("Location: ../?err=session_expired");
}
?>
<?php
include "../process/CollegeDAO.php";

$process = new CollegeDAO();

$data = $process->getEventData($_GET["event_id"]);
$college_data = $process->getCollegeData($data["fk_id"]);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | College</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-4">
            <h3>Exemption</h3>
            <h4><?php echo ucwords($data["name"])?> (<?php echo ucwords($college_data["acronym"])?>)</h4>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <input type="hidden" class="event_id_fld" name="event_id" value="<?php echo $_GET["event_id"];?>"/>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered student-list">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Student ID</th>
                            <th>Names</th>
                            <th>Course</th>
                            <th style="width: 50px;">Time-in</th>
                            <th style="width: 50px;">Time-out</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                           <?php
                                $process->viewStudentList($_GET["event_id"], $college_data["acronym"]);
                           ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="modal" id="addExemption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b style="font-size: 12px;" class="modal-title ex-title" id="myModalLabel"></b>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" style="font-size: 12px;">
                        <tr>
                            <th>Time-in</th>
                            <th>Time-out</th>
                        </tr>
                        <tr>
                            <td class="disp-t1"><button class="btn btn-primary btn-xs">Time in now</button></td>
                            <td class="disp-t2"><button class="btn btn-primary btn-xs">Time out now</button></td>
                        </tr>
                    </table>
                </div>
                <!--<div class="modal-footer">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/college/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                            $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>