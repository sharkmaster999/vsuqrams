<?php
function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip=$_SERVER['SERVER_ADDR'];
    }
    return $ip;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title>Visayas State University QR Code Attendance Monitoring System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/bootstrap-customize.css"/>
        <link rel="stylesheet" href="css/bootstrap-datepicker3.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.css"/>
        <link rel="stylesheet" href="css/style.css"/>
    </head>
    <body>
        <?php include "navigation.php";?>
        <div class="container">
            <div class="row" style="margin-top: 40px;">
                <div class="col-md-4 col-md-offset-4">
                    <div class="well" style="padding-bottom: 0; width: 100%">
                        <div>
                            <p class="text-center">
                                Shared Address: <br/>
                                <b>https://<?php echo getRealIpAddr();?>/vsuattendance</b></p>
                        </div>
                        <div style="margin: 0 auto; width: 90px; margin-top: 10px;">
                            <img src="img/ussc-logo.png" alt="" style="width: 90px;"/>
                        </div>
                        <h1 class="text-center" style="color: #005216; line-height: 0.8">
                            <span style="font-size: 25px;"><b>Visayas State University</b></span> <br>
                            <span style="font-size: 20px;"><b>University Supreme Student Council</b></span> <br>
                            <span style="font-size: 13px;">Quick Response Student Attendance Monitoring System</span>
                        </h1>
                        <br>
                        <div class="row" style="border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;">
                            <div class="col-md-12" style="padding-top: 15px;">
                                <form action="controllers/users/LoginModule.php" method="POST">
                                    <div class="form-group">
                                        <label for="student_id">Authorized Student ID Number:</label>
                                        <input type="text" name="student_id" id="student_id" class="form-control" autocomplete="off"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input type="password" name="password" id="password" class="form-control" autocomplete="off"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Login to Account</button>
                                </form>
                                <br>
                            </div>
                        </div>
                        <p class="text-center" style="font-size: 11px; color: #777; margin-top: 10px;">&copy; USSC-QRSAMS. All rights reserved. Developed by Remuelito.</p>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/filereader.js"></script>
        <!-- Using jquery version: -->

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="js/qrcodelib.js"></script>
        <script type="text/javascript" src="js/webcodecamjquery.js"></script>
        <script type="text/javascript" src="js/mainjquery.js"></script>
    </body>
</html>