$(function(){
    $(".add-college-btn").on("click", function(){
        $(".college-frm").submit();
    });

    $(".add-event-btn").on("click", function(){
        $(".event-frm").submit();
    });

    $('.event-date').datepicker({
        format: "yyyy-mm-dd"
    });

    $(".add-dept-btn").on("click", function () {
        $(".dept-frm").submit();
    });

    $(".add-semester-btn").on("click", function () {
        $(".semester-frm").submit();
    });
    
    $(".add-semester1-btn").on("click", function () {
        $(".semester1-frm").submit();
    });

    $(".add-payment-btn").on("click", function () {
        $(".payment-frm").submit();
    })
});