$(function () {
    $(".edit-server-btn").on("click", function () {
        $(".server-frm").submit();
    });

    $(".add-student-btn").on("click", function () {
        $(".student-frm").submit();
    });

    $(".import-btn").on("click", function () {
        $(".import-frm").submit();
    });

    $(".student-list").dataTable({
        "bSort": false
    });
    $(".generateqr-btn").on("click", function () {
        $(".generateqr-frm").submit();
    });

    $(".export-time-btn").on("click", function () {
        $(".export-time-frm").submit();
    });

    var arr = new Array();
    var i = 0;
    $(document).on("click", ".student_qr", function () {

        if($(this).val() == 0) {
            arr[i] = $(this).data("id");
            i++;
            $(this).val(1);
            $(".noqr_stud").val(arr);
        } else {
            var deleted_index = arr.indexOf($(this).data("id"));
            delete arr[deleted_index];
            $(".noqr_stud").val(arr);
            i--;
            $(this).val(0);
        }
    });

    $(".exemption_btn").on("click", function () {
        var exempt = $(this).data("content");

        $(".exempt").val(exempt);
        $(".exemption_frm").submit();
    });

    $(document).on("click", ".exempt-student-btn", function () {
        var event_id = $(this).data("event");
        var log_type = $(this).data("log");
        var stud_id = $(this).data("studid");

        $.ajax({
            url: "../controllers/administrator/AddTimeLogOnExemption.php",
            type: "POST",
            dataType: "html",
            data: {event_id: event_id, log_type: log_type, stud_id: stud_id},
            success:function(e) {
                if(log_type == "Time-in") {
                    $(".timein-"+stud_id).remove();
                } else if(log_type == "Time-out") {
                    $(".timeout-"+stud_id).remove();
                } else if(log_type == "Exempted") {
                    $(".exempted-"+stud_id).remove();
                }
            }
        });
    });

    $(document).on("click", ".exempt-stud", function () {
        var stud_id = $(this).data("id");
        var fullname = $(this).data("fullname");
        var event_id = $(".event_id_fld").val();

        $(".disp-t1").empty();
        $(".disp-t2").empty();

        $(".ex-title").empty();
        $(".ex-title").append(stud_id + " - " + fullname);

        $.ajax({
            url: "../controllers/administrator/CheckAttendance.php",
            type: "POST",
            dataType: "html",
            data: {student_id: stud_id, event_id: event_id, log_type: "Time-in"},
            success: function(e) {
                if(e != "") {
                    $(".disp-t1").html(e);
                } else {
                    var str = '<button class="btn btn-primary btn-sm exempt-student-btn timein-'+stud_id+'" data-event="'+ event_id +'" data-log="Time-in" data-studid="'+ stud_id +'">Time-in</button>';
                    $(".disp-t1").html(str);
                }
            }
        });

        $.ajax({
            url: "../controllers/administrator/CheckAttendance.php",
            type: "POST",
            dataType: "html",
            data: {student_id: stud_id, event_id: event_id, log_type: "Time-out"},
            success: function(e) {
                if(e != "") {
                    $(".disp-t2").html(e);
                } else {
                    var str = '<button class="btn btn-primary btn-sm exempt-student-btn timeout-'+stud_id+'" data-event="'+ event_id +'" data-log="Time-out" data-studid="'+ stud_id +'">Time-out</button>';
                    $(".disp-t2").html(str);
                }
            }
        });
        $("#addExemption").modal("show");
    });

    $(document).on("click", ".payment-stud", function () {
        var stud_id = $(this).data("id");
        var fullname = $(this).data("fullname");
        var payment_id = $("#payment_id").val();
        var o_type = $("#o_type").val();

        $.ajax({
            url: "../controllers/administrator/CheckIfPaidPayment.php",
            type: "POST",
            dataType: "html",
            data: {type: o_type, payment_id: payment_id, student_id: stud_id},
            success: function(e) {
                if(e == 0) {
                    $(".pay-title").empty();
                    $(".pay-title").append(stud_id + " - " + fullname);
                    $("#amt-tnd-cls").empty();
                    $("#amt-tnd-cls").append("0.00");
                    $("#or_number").val("");
                    $(".print-pay").empty();
                    $("#amount_pay").val("");
                    $("#student_id").val(stud_id);
                    $("#addPaymentLog").modal("show");
                } else {
                    $(".pay-title1").empty();
                    $(".pay-title1").append(stud_id + " - " + fullname);
                    var append_html = "<a href='print_pay_log.php?id="+e+"' class='btn btn-primary btn-sm'><i class='fa fa-print'></i> Re-print Receipt</a>" +
                                      "<button class='btn btn-danger btn-sm remove-pay-log' data-id='"+e+"'><i class='fa fa-trash-o'></i> Remove</button>"
                    $(".print-pay1").empty();
                    $(".print-pay1").append(append_html);
                    $("#alreadyPaid").modal("show");
                }
            }
        });
    });

    $(document).on("keyup", "#amount_pay", function () {
        var amt_pay = $("#amount_pay").val();
        var total_pay = $("#total_pay").val();
        var amt_tender = amt_pay-total_pay;
        $("#amt-tnd-cls").empty();
        $("#amt-tnd-cls").append(amt_tender + ".00");
        $("#amt_tender").val(amt_tender)
    })

    $(".save-payment-btn").click(function () {
        var student_id = $("#student_id").val();
        var or_number = $("#or_number").val();
        var total_pay = $("#total_pay").val();
        var amt_pay = $("#amount_pay").val();
        var amt_tender = $("#amt_tender").val();
        var o_type = $("#o_type").val();
        var payment_id = $("#payment_id").val();

        $.ajax({
            url: "../controllers/administrator/SetPaymentManual.php",
            type: "POST",
            dataType: "html",
            data: {student_id : student_id, or_number : or_number, total_pay : total_pay, amt_pay: amt_pay, amt_tender: amt_tender, o_type: o_type, payment_id: payment_id},
            success: function(e) {
                var append_html = "<a href='print_pay_log.php?id="+e+"' class='btn btn-primary btn-sm'><i class='fa fa-print'></i> Print Now</a>"
                $(".print-pay").empty();
                $(".print-pay").append(append_html);
            }
        });
    });

    $(document).on("click", ".remove-pay-log", function () {
        var id = $(this).data("id");
        $.ajax({
            url: "../controllers/administrator/DeletePaymentManual.php",
            type: "POST",
            dataType: "html",
            data: {payment_id : id},
            success: function(e) {
                $("#alreadyPaid").modal("hide");
            }
        });
    })

});