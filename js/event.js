$(function () {
    $(".add-event-btn").click(function () {
         $(".event-frm").submit();
    });

    $(".export-data-btn").click(function () {
        $(".export-data-frm").submit();
    });

    $(".trigger_log").on("click", function() {
        $(".active_log").empty();

        $(".active_log").text($(this).val());

        $("#Status").val($(this).val());
    });

    $('.birthdate').datepicker({
        format: "yyyy-mm-dd"
    });

    $(document).on("click", ".delete-log", function () {
        var id = $(this).data("id");

        var confirmation = confirm("Are you sure you want to delete the log you selected?");

        if(confirmation) {
            $.ajax({
                url: "../controllers/administrator/DeleteTimeLog.php",
                type: "POST",
                dataType: "html",
                data: {log_id : id},
                success: function(e) {
                    if(e == "Success") {
                        $(".timelog-"+id).remove();
                    }
                }
            });
        }
    });
});