<?php

include "ConnectionDAO.php";

class CollegeDAO extends ConnectionDAO {
	function getCollegeData($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeEvent($college_id, $year, $semester) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_event WHERE type = 'College' AND fk_id = ? AND year = ? AND semester = ? ORDER BY id DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->bindParam(2, $year);
            $stmt->bindParam(3, $semester);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. $row["name"].'</span>
                            <span style="display: block"><span class="glyphicon glyphicon-calendar"></span> '. date('l, F d, Y', strtotime($row["date"])).'</span>
                            <a href="operate_event.php?event_id='. $row["id"]. '" class="btn btn-primary btn-xs">Start Operate</a>
                        </div>
                     </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addDepartment($name, $course_name, $dept_acro, $course_acro, $course_major, $college_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_department VALUES (0, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->bindParam(2, $name);
            $stmt->bindParam(3, $course_name);
            $stmt->bindParam(4, $dept_acro);
            $stmt->bindParam(5, $course_acro);
            $stmt->bindParam(6, $course_major);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewDepartment($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_department WHERE college_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. $row["name"].'</span>
                            <span style="display: block"><span class="fa fa-graduation-cap"></span> '. $row["course_name"].'</span>
                        </div>
                     </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getEventData($event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_event WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getTimeLogData($event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE event_id = ? ORDER BY id DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $data = $this->getUserData($row["student_id"]);
                if(sizeof($data) != 1) {
                    echo '<tr class="timelog-'. $row["id"].'">
                                <td>'. $row["student_id"].'</td>
                                <td>'. $data["lastname"].', '. $data["firstname"].' '. $data["middlename"].'</td>
                                <td>'. $data["course"].'-'. $data["level"].'</td>
                                <td>'. $row["status"].'</td>
                                <td>'. date('F d, Y h:i:s A', strtotime($row["time_enter"])).'</td>
                                <td><button class="btn btn-danger btn-xs delete-log" data-id="'. $row["id"].'">Cancel</button></td>
                          </tr>';
                } else {
                    echo 1;
                }

            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getUserData($student_id) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["level"] = $row["level"];
            }

            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getStudentTimeInExemption($event_id, $log_type, $student_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE event_id = ? AND status = ? AND student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->bindParam(2, $log_type);
            $stmt->bindParam(3, $student_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();
            return $res;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function viewStudentList($event_id, $college) {
        try {
            $this->openConnection();

            if($college == "ALL") {
                $sql = "SELECT * FROM tbl_user ORDER BY lastname";
                $stmt = $this->dbh->prepare($sql);
                $stmt->execute();
            }
            else {
                $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY lastname";
                $stmt = $this->dbh->prepare($sql);
                $stmt->bindParam(1, $college);
                $stmt->execute();
            }
            

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . " ". $row["major"];
                }
                echo '<tr class="exempt-stud" style="cursor: pointer;" data-id="'. $row['stud_num']. '" data-fullname="'. $row["lastname"].', '. $row["firstname"]. ' '. $row["middlename"]. '">
                        <td style="width: 5px;"><!-- <input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"> --></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).' '. ucfirst($row["middlename"]).'</td>
                        <td>'. $row["course"].'-'. $row["level"].'</td>';
                echo'</tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
    
    function getSchoolYearOption() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr ORDER BY year, semester DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $sem = "";
                if($row["semester"] == 1) {
                    $sem = "1st Semester";
                } elseif ($row["semester"] == 2) {
                    $sem = "2nd Semester";
                } elseif ($row["semester"] == 3) {
                    $sem = "Summer";
                }
                echo '<option value="'. $row["id"]. '">'. $row["year"]. '-'. ($row["year"] + 1). ' '. $sem. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getActiveSemester($id) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["year"] = $row["year"];
                $arr["semester"] = $row["semester"];
            }

            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addEvent($name, $year, $semester, $fines, $auth_id, $date, $type, $fk_id, $sign_ctr) {
        try {
            $this->openConnection();

            $sql = " INSERT INTO tbl_event VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $year);
            $stmt->bindParam(3, $semester);
            $stmt->bindParam(4, $fines);
            $stmt->bindParam(5, $auth_id);
            $stmt->bindParam(6, $date);
            $stmt->bindParam(7, $type);
            $stmt->bindParam(8, $fk_id);
            $stmt->bindParam(9, $sign_ctr);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeStudents($college_acro) {
        try {
            $this->openConnection();

        
            $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_acro);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . " ". $row["major"];
                }
                echo '
                <tr>
                    <td style="width: 5px;"><input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"></td>
                    <td>'. $row["stud_num"].'</td>
                    <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).' '. ucfirst($row["middlename"]).'</td>
                    <td>'. $row["course"].'-'. $row["level"].'</td>
                </tr>
                ';
            }

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getDataByStudentID($student_id) {
        try {
            $result[] = null;
            $this->openConnection();
            $id = explode(",", $student_id);
            $y = 0;
            foreach($id as $row) {
                if($row != null || $row != "") {
                    $data[] = null;
                    $sql = "SELECT * FROM tbl_user WHERE id = ?";
                    $stmt = $this->dbh->prepare($sql);
                    $stmt->bindParam(1, $row);
                    $stmt->execute();

                    $i = 0;
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $arr[] = null;
                        $arr["student_id"] = $row["stud_num"];
                        $arr["lastname"] = $row["lastname"];
                        $arr["firstname"] = $row["firstname"];
                        $arr["middlename"] = $row["middlename"];
                        $arr["course"] = $row["course"];
                        $arr["college"] = $row["college"];
                        $arr["major"] = $row["major"];
                        $data[$i] = $arr;
                        $i++;
                    }
                    $result[$y] = $data;
                    $y++;
                }
            }

            $this->closeConnection();
            return $result;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getInfoByStudentID($student_id) {
        try {
            $this->openConnection();
            $arr[] = null;
            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["student_id"] = $row["stud_num"];
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["college"] = $row["college"];
                $arr["major"] = $row["major"];
            }

            $this->closeConnection();
            return $arr;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getDataByCollege($college) {
        try {
            $data[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college);
            $stmt->execute();

            $i = 0;
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr[] = null;
                $arr["student_id"] = $row["stud_num"];
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"]. " ". $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["major"] = $row["major"];
                $arr["college"] = $row["college"];
                $data[$i] = $arr;
                $i++;
            }

            $this->closeConnection();

            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getOrganizationName($type, $fk_id) {
        try {
            $this->openConnection();

            $sql = "";

            if($type == "College") {
                $sql = "SELECT name FROM tbl_college WHERE id = ?";
            } elseif($type == "Department") {
                $sql = "SELECT name FROM tbl_department WHERE id = ?";
            }
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $fk_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            if($type != "University") {
                return $res["name"];
            } else {
                return "University Account";
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewDepartmentSelection($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_department WHERE college_id = ? ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<option value="'. $row["id"]. '">'. $row["name"]. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addAccount($student_id, $password, $org_type, $fk_id, $status) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_auth_user VALUES(0, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $password);
            $stmt->bindParam(3, $org_type);
            $stmt->bindParam(4, $fk_id);
            $stmt->bindParam(5, $status);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getUserAccounts($org_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_auth_user WHERE type = 'College' AND fk_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $org_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $auth_type = "";

                if($row["status"] == 0) {
                    $auth_type = "Administrator";
                } else {
                    $auth_type = "Encoder";
                }

                $user_data = $this->getInfoByStudentID($row["student_id"]);
                echo '<tr>
                        <td>'. $row["student_id"]. '</td>
                        <td><b>'. $user_data["lastname"]. '</b>, '. $user_data["firstname"]. '</td>
                        <td>'. $this->getOrganizationName($row["type"], $row["fk_id"]). '</td>
                        <td>'. $row["type"]. ' - '. $auth_type. '</td>
                        <td>
                            <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Remove</a>
                        </td>
                    </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeSelection() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<option value="'. $row["id"]. '">'. $row["name"]. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getAccountLastName($student_id) {
        try {
            $this->openConnection();
            $sql = "SELECT lastname FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();
            return $res["lastname"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewPayment($type, $fk_id, $year, $semester) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment WHERE type = ? AND year = ? AND semester = ? AND fk_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $type);
            $stmt->bindParam(2, $year);
            $stmt->bindParam(3, $semester);
            $stmt->bindParam(4, $fk_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <b>'. $row["name"]. '</b> <br/>
                            <span style="display: block">Amount: '. $row["amount"]. '</span>
                            <a href="operate_payment.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">QR Collect</a>
                            <a href="manual_payment.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">Manual Collect</a>
                            <a href="generate_payment_report.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">Generate Report</a>
                            <a href="#" class="btn btn-danger btn-xs">Remove</a>
                        </div>
                    </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addPayment($name, $description, $amount, $year, $semester, $auth_id, $type, $fk_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_payment VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $description);
            $stmt->bindParam(3, $amount);
            $stmt->bindParam(4, $year);
            $stmt->bindParam(5, $semester);
            $stmt->bindParam(6, $auth_id);
            $stmt->bindParam(7, $type);
            $stmt->bindParam(8, $fk_id);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentData($payment_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentLogByPaymentID($payment_id, $type) {

        try {
            $this->openConnection();

            $sql1 = "SELECT p.id AS p_id, p.student_id, u.lastname, u.firstname, u.course, u.major, u.college, p.or_number, p.total_pay, p.date_pay
                     FROM tbl_payment_log p, tbl_user u WHERE p.student_id = u.stud_num AND p.fk_id = ? AND p.type = ?
                     ORDER BY p.id DESC";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $payment_id);
            $stmt1->bindParam(2, $type);
            $stmt1->execute();

            while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }
                echo '<tr class="payment-'. $row["p_id"]. '">
                          <td>'. $row["student_id"]. '</td>
                          <td>'. $row["lastname"]. ', '. $row["firstname"]. '</td>
                          <td>'. $row["course"]. '</td>
                          <td>Paid</td>
                          <td>'. date('F d, Y', strtotime($row["date_pay"])).'</td>
                          <td>
                              <a class="btn btn-primary btn-xs" target="_blank" href="print_pay_log.php?id='. $row["p_id"]. '"><i class="fa fa-print"></i> Re-print O.R.</a>
                              <button class="btn btn-danger btn-xs remove-pay-log" data-id="'. $row["p_id"]. '"><i class="fa fa-trash-o"></i> Remove</button>
                          </td>
                      </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function deletePaymentLogByID($pay_log_id) {
        try {
            $this->openConnection();

            $sql1 = "DELETE FROM tbl_payment_log WHERE id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $pay_log_id);
            $stmt1->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function viewStudentListForCollection($event_id, $college) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . " ". $row["major"];
                }
                echo '<tr class="collect-stud" style="cursor: pointer;" data-id="'. $row['stud_num']. '" data-fullname="'. $row["lastname"].", ". $row["firstname"]. '">
                        <td style="width: 5px;"><!-- <input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"> --></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).' '. ucfirst($row["middlename"]).'</td>
                        <td>'. $row["course"].'-'. $row["level"].'</td>';
                echo'</tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function checkTimeLogJSONTick($student_id, $log_type, $event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT time_enter FROM tbl_time_log WHERE student_id = ? AND status = ? AND event_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $log_type);
            $stmt->bindParam(3, $event_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            if($res["time_enter"] != "")
                return 1;
            else
                return 0;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getPaidFinesByID($student_id, $type, $event_id) {
        try {
            $this->openConnection();
            $paid_fines = 0;
            $sql = "SELECT * FROM tbl_fines_log WHERE type = ? AND fk_id = ? AND student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $type);
            $stmt->bindParam(2, $event_id);
            $stmt->bindParam(3, $student_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $paid_fines += $row["total_pay"];
            }

            $this->closeConnection();

            return $paid_fines;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentByStudent($student_id, $year, $sem, $type, $fk_id) {

        try {
            $this->openConnection();

            $total_fines = 0;
            $total_paid = 0;

            $sql = "SELECT * FROM tbl_event WHERE year = ? AND semester = ? AND type = ? AND fk_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $year);
            $stmt->bindParam(2, $sem);
            $stmt->bindParam(3, $type);
            $stmt->bindParam(4, $fk_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $fines = $row["fines"];
                $sign_ctr = $row["sign_ctr"];
                $ti = $this->checkTimeLogJSONTick($student_id, "Time-in", $row["id"]);
                $to = $this->checkTimeLogJSONTick($student_id, "Time-out", $row["id"]);
                $per_fines = $fines / $sign_ctr;
                $total_sign = $ti + $to;

                $paid_fines = $this->getPaidFinesByID($student_id, $type, $row["id"]);

                if($total_sign > $sign_ctr)
                    $total_sign = $sign_ctr;
                $less_fines = $per_fines * $total_sign;
                $fines = $fines - $less_fines;

                echo '<tr>
                        <td>'. $row["name"]. ' [P '. $row["fines"]. '.00 per '. $row["sign_ctr"]. ' sign(s)]</td>
                        <td>'. $ti. '</td>
                        <td>'. $to. '</td>
                        <td><b>P '. $fines. '.00</b></td>
                        <td><b>P '. $paid_fines. '.00</b></td>
                    </tr>';
                $total_fines = $total_fines + $fines;
                $total_paid = $total_paid + $paid_fines;
            }


            $str_pay_btn = "";
            $total_fines = $total_fines - $total_paid;

            if($total_fines > 0)
                $str_pay_btn = "<a href='#' class='btn btn-primary btn-xs collect-fines' data-org='College'
                                            data-kind='Fines' data-studid='". $student_id. "'><i class='fa fa-dollar'></i> Pay Now</a>";


            echo '<tr>
                    <td></td>
                    <td colspan="2"><b>Total Fines:</b> </td>
                    <td><b style="font-size: 14px;" id="tp_fines">P '. $total_fines. '.00</b></td>
                    <td id="tp_fines_btn">'. $str_pay_btn. '</td>
                </tr>';

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function checkPaymentLogByPaymentID($student_id, $type, $payment_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment_log WHERE student_id = ? AND type = ? AND fk_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $type);
            $stmt->bindParam(3, $payment_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getUniversityPaymentByStudent($student_id, $year, $sem) {

        try {
            $this->openConnection();

            $total_payment = 0;

            $sql = "SELECT * FROM tbl_payment WHERE year = ? AND semester = ? AND type = 'University'";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $year);
            $stmt->bindParam(2, $sem);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

                $payment_log = $this->checkPaymentLogByPaymentID($student_id, 'University', $row["id"]);

                if($payment_log["total_pay"] == "")
                    $paid_amt = 0.00;
                else
                    $paid_amt = $payment_log["total_pay"];

                $pa = $row["amount"] - $paid_amt;

                echo '<tr>
                        <td>'. $row["name"]. '</td>
                        <td>'. $row["amount"]. '</td>
                        <td>'. $paid_amt. '</td>
                        <td><b>'. $pa. '.00</b></td>
                        <td></td>
                    </tr>';
                $total_payment = $total_payment + $pa;
            }


            $str_pay_btn = "";

            if($total_payment > 0)
                $str_pay_btn = "<a href='#' class='btn btn-primary btn-xs collect-payment' data-org='University'
                                            data-kind='Payment' data-studid='". $student_id. "'><i class='fa fa-dollar'></i> Pay Now</a>";

            echo '<tr>
                    <td></td>
                    <td colspan="2"><b>Total Payment:</b> </td>
                    <td><b style="font-size: 14px;" id="tp_univ">P '. $total_payment. '.00</b></td>
                    <td id="tp_univ_btn">'. $str_pay_btn. '</td>
                </tr>';

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getCollegePaymentByStudent($student_id, $year, $sem, $fk_id) {

        try {
            $this->openConnection();

            $total_payment = 0;

            $sql = "SELECT * FROM tbl_payment WHERE year = ? AND semester = ? AND type = 'College' AND fk_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $year);
            $stmt->bindParam(2, $sem);
            $stmt->bindParam(3, $fk_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

                $payment_log = $this->checkPaymentLogByPaymentID($student_id, 'College', $row["id"]);

                if($payment_log["total_pay"] == "")
                    $paid_amt = 0.00;
                else
                    $paid_amt = $payment_log["total_pay"];

                $pa = $row["amount"] - $paid_amt;

                echo '<tr>
                        <td>'. $row["name"]. '</td>
                        <td>'. $row["amount"]. '</td>
                        <td>'. $paid_amt. '</td>
                        <td><b>'. $pa. '.00</b></td>
                        <td></td>
                    </tr>';
                $total_payment = $total_payment + $pa;
            }

            $str_pay_btn = "";

            if($total_payment > 0)
                $str_pay_btn = "<a href='#' class='btn btn-primary btn-xs collect-payment-col' data-org='College'
                                            data-kind='Payment' data-studid='". $student_id. "'><i class='fa fa-dollar'></i> Pay Now</a>";

            echo '<tr>
                    <td></td>
                    <td colspan="2"><b>Total Payment:</b> </td>
                    <td><b style="font-size: 14px;" id="tp_col">P '. $total_payment. '.00</b></td>
                    <td id="tp_col_btn">'. $str_pay_btn. '</td>
                </tr>';

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewAllStudentListForPayment($college_acro) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_acro);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }
                echo '
                    <tr class="payment-stud" style="cursor: pointer;" data-id="'. $row['stud_num']. '" data-fullname="'. $row["lastname"].", ". $row["firstname"]. '">
                        <td style="width: 5px;"><!-- <input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"> --></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).'</td>
                        <td>'. $row["course"].'</td>
                    </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function checkIfExistPayment($stud_num, $payment_id, $type) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment_log WHERE fk_id = ? AND type = ? AND student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->bindParam(2, $type);
            $stmt->bindParam(3, $stud_num);
            $stmt->execute();
            $i = false;
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $i = true;
            }

            $this->closeConnection();

            return $i;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addPaymentLog($stud_num, $or_number, $total_pay, $amt_pay, $amt_tender, $type, $payment_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_payment_log VALUES(0, ?, ?, ?, ?, ?, now(), ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $stud_num);
            $stmt->bindParam(2, $or_number);
            $stmt->bindParam(3, $total_pay);
            $stmt->bindParam(4, $amt_pay);
            $stmt->bindParam(5, $amt_tender);
            $stmt->bindParam(6, $type);
            $stmt->bindParam(7, $payment_id);
            $stmt->execute();

            $sql1 = "SELECT * FROM tbl_payment_log WHERE student_id = ? AND type = ? AND fk_id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $stud_num);
            $stmt1->bindParam(2, $type);
            $stmt1->bindParam(3, $payment_id);
            $stmt1->execute();

            $res = $stmt1->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res["id"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function payFinesByEvent($student_id, $fines, $auth_type, $event_id) {
        try {
            $this->openConnection();

            $sql1 = "INSERT INTO tbl_fines_log VALUES (0, ?, '', ?, ?, 0.00, now(), ?, ?)";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $student_id);
            $stmt1->bindParam(2, $fines);
            $stmt1->bindParam(3, $fines);
            $stmt1->bindParam(4, $auth_type);
            $stmt1->bindParam(5, $event_id);
            $stmt1->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
    function payAllPaymentByType($student_id, $auth_type, $type, $year, $sem, $fk_id) {
        try {
            $this->openConnection();

            if($type == "Payment") {
                $sql = "SELECT * FROM tbl_payment WHERE year = ? AND semester = ? AND type = ?";
                $stmt = $this->dbh->prepare($sql);
                $stmt->bindParam(1, $year);
                $stmt->bindParam(2, $sem);
                $stmt->bindParam(3, $auth_type);
                $stmt->execute();

                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $if_paid = $this->checkIfExistPayment($student_id, $row["id"], $auth_type);

                    if(!$if_paid) {
                        $this->addPaymentLog($student_id, '', $row["amount"], $row["amount"], 0.00, $auth_type, $row["id"]);
                    }
                }
            } elseif($type == "Fines") {
                $sql = "SELECT * FROM tbl_event WHERE year = ? AND semester = ? AND type = ? AND fk_id = ?";
                $stmt = $this->dbh->prepare($sql);
                $stmt->bindParam(1, $year);
                $stmt->bindParam(2, $sem);
                $stmt->bindParam(3, $auth_type);
                $stmt->bindParam(4, $fk_id);
                $stmt->execute();

                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $fines = $row["fines"];
                    $sign_ctr = $row["sign_ctr"];
                    $ti = $this->checkTimeLogJSONTick($student_id, "Time-in", $row["id"]);
                    $to = $this->checkTimeLogJSONTick($student_id, "Time-out", $row["id"]);
                    $per_fines = $fines / $sign_ctr;
                    $total_sign = $ti + $to;

                    if($total_sign > $sign_ctr)
                        $total_sign = $sign_ctr;
                    $less_fines = $per_fines * $total_sign;
                    $fines = $fines - $less_fines;

                    $paid_fines = $this->getPaidFinesByID($student_id, $auth_type, $row["id"]);

                    if($paid_fines != $fines) {
                        $this->payFinesByEvent($student_id, $fines, $auth_type, $row["id"]);
                    }

                }
            }


            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addTimeLogData($student_id, $event_id, $status, $time_log) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_time_log VALUES(0, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $event_id);
            $stmt->bindParam(3, $status);
            $stmt->bindParam(4, $time_log);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}