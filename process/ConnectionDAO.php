<?php 

class ConnectionDAO {
	protected $host = "localhost";
	protected $username = "root";
	protected $password = "";
	protected $db_name = "vsuqrams";
	protected $dbh = null;
	
	public function openConnection() {
		try {
			$this->dbh = new PDO("mysql:host=". $this->host. ";dbname=".$this->db_name, $this->username, $this->password);
			
			return $this->dbh;
		} catch(Exception $e) {
			$e->getMessage();
		}
	}
	
	public function closeConnection() {
		try {
			$this->dbh = null;
			
			return $this->dbh;
		} catch(Exception $e) {
			$e->getMessage();
		}
	}
}

?>