<?php

include_once "ConnectionDAO.php";

class AdministratorDAO extends ConnectionDAO {
    function addCollege($name, $acronym) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_college VALUES (0, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $acronym);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollege() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. ucwords($row["name"]).' <br>('. strtoupper($row["acronym"]).')</span>
                            <a href="college_info.php?id='. $row["id"].'" class="btn btn-primary btn-xs">View Details</a>
                            <!-- <a href="#" class="btn btn-danger btn-xs">Remove</a> -->
                        </div>
                      </div>';
            }
            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeList() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. ucwords($row["name"]).' <br>('. strtoupper($row["acronym"]).')</span>
                            <a href="college_details.php?id='. $row["id"].'" class="btn btn-primary btn-xs">View Details</a>
                            <!-- <a href="#" class="btn btn-danger btn-xs">Remove</a> -->
                        </div>
                      </div>';
            }
            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getCollegeData($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getCollegeDataByAcronym($college_acro) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college WHERE acronym = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_acro);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeEvent($college_id, $school_period, $year) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_event WHERE type = 'College' AND fk_id = ? AND semester = ? AND year = ? ORDER BY id DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->bindParam(2, $school_period);
            $stmt->bindParam(3, $year);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. $row["name"].'</span>
                            <span style="display: block"><span class="glyphicon glyphicon-calendar"></span> '. date('l, F d, Y', strtotime($row["date"])).'</span>
                            <a href="operate_event.php?event_id='. $row["id"]. '" class="btn btn-primary btn-xs">Operate</a>
                            <a href="exemption.php?event_id='. $row["id"]. '" class="btn btn-primary btn-xs">Exemption</a>
                        </div>
                     </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getEventData($event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_event WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getTimeLogData($event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE event_id = ? ORDER BY id DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $data = $this->getUserData($row["student_id"]);

                if(sizeof($data) != 1) {
                    echo '<tr class="timelog-'. $row["id"].'">
                                <td>'. $row["student_id"].'</td>
                                <td>'. $data["lastname"].', '. $data["firstname"].' '. $data["middlename"].'</td>
                                <td>'. $data["course"].'</td>
                                <td>'. $row["status"].'</td>
                                <td>'. date('F d, Y h:i:s A', strtotime($row["time_enter"])).'</td>
                                <td><button class="btn btn-danger btn-xs delete-log" data-id="'. $row["id"].'">Cancel</button></td>
                          </tr>';
                }

            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getUserData($student_id) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
            }

            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function counterData($student_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();

            $i = 0;

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $i++;
            }

            $this->closeConnection();

            return $i;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function checkIfExistTime($student_number, $status, $event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE student_id = ? AND status = ? AND event_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->bindParam(2, $status);
            $stmt->bindParam(3, $event_id);
            $stmt->execute();

            $i = 0;

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $i++;
            }

            $this->closeConnection();

            return $i;
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    function getDataByCode($student_number, $status, $event_id, $ctr) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->execute();
            $arr["log_time"] = false;
            if($ctr != 1) {
                $this->timeLog($student_number, $status, $event_id);
                $arr["log_time"] = true;
                $time_log = $this->getTimeLog($student_number, $status, $event_id);
                $arr["time_log"] = date('F d, Y h:i:s A', strtotime($time_log["time_enter"]));
                $arr["log_id"] = $time_log["id"];
            }

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["student_number"] = $row["stud_num"];
                $arr["image"] = $row["image"];
            }

            $this->closeConnection();

            return $arr;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getDataPaymentByCode($student_number, $payment_id, $type, $total_pay) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql1 = "INSERT INTO tbl_payment_log VALUES (0, ?, '', ?, ?, 0.00, now(), ?, ?)";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $student_number);
            $stmt1->bindParam(2, $total_pay);
            $stmt1->bindParam(3, $total_pay);
            $stmt1->bindParam(4, $type);
            $stmt1->bindParam(5, $payment_id);
            $stmt1->execute();

            $arr = $this->getStudentPaymentLogArr($payment_id, $type, $student_number);

            $this->closeConnection();

            return $arr;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getStudentPaymentLogArr($payment_id, $type, $student_number) {
        try {
            $arr[] = null;
            $sql = "SELECT p.id AS p_id, p.student_id, u.lastname, u.firstname, u.course, u.major, u.college, u.image, p.or_number, p.total_pay, p.date_pay
                     FROM tbl_payment_log p, tbl_user u WHERE p.student_id = u.stud_num AND p.fk_id = ? AND p.type = ? AND p.student_id = ?
                     ORDER BY p.id DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->bindParam(2, $type);
            $stmt->bindParam(3, $student_number);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["course"] = $row["course"];
                $arr["college"] = $row["college"];
                $arr["student_number"] = $row["student_id"];
                $arr["image"] = $row["image"];
                $arr["total_pay"] = $row["total_pay"];
                $arr["date_pay"] = $row["date_pay"];
                $arr["p_id"] = $row["p_id"];
            }
            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getTimeLog($student_number, $status, $event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE student_id = ? AND status = ? AND event_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->bindParam(2, $status);
            $stmt->bindParam(3, $event_id);
            $stmt->execute();

            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (PDOException $e) {
            $e->getMessage();
        }
    }

    function timeLog($student_number, $status, $event_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_time_log VALUES(0, ?, ?, ?, now())";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->bindParam(2, $event_id);
            $stmt->bindParam(3, $status);
            $stmt->execute();

            $this->closeConnection();

            return 0;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function deleteTimeLog($log_id) {
        try {
            $this->openConnection();

            $sql = "DELETE FROM tbl_time_log WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $log_id);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeStudents($college_id) {
        try {
            $this->openConnection();

            if($college_id != 0) {
                $sql = "SELECT * FROM tbl_department WHERE college_id = ? ORDER by course_acro";
                $stmt = $this->dbh->prepare($sql);
                $stmt->bindParam(1, $college_id);
                $stmt->execute();

                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $this->viewStudentList($row["course_acro"]);
                }
            } else {
                $sql = "SELECT * FROM tbl_user ORDER BY lastname";
                $stmt = $this->dbh->prepare($sql);
                $stmt->execute();

                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if($row["major"] != " ") {
                        $row["course"] = $row["course"] . " " . $row["major"];
                    }
                    echo '
                    <tr>
                        <td style="width: 5px;"><input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).'  '. ucfirst($row["middlename"]).' </td>
                        <td>'. $row["course"]. "-" . $row["level"] . '</td>
                    </tr>
                    ';
                }
            }

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getStudentTimeInExemption($event_id, $log_type, $student_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_time_log WHERE event_id = ? AND status = ? AND student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $event_id);
            $stmt->bindParam(2, $log_type);
            $stmt->bindParam(3, $student_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();
            return $res;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function viewStudentList($college) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . " ". $row["major"];
                }
                echo '
                    <tr class="exempt-stud" style="cursor: pointer;" data-id="'. $row['stud_num']. '" data-fullname="'. $row["lastname"].', '. $row["firstname"]. ' '. $row["lastname"]. '">
                        <td style="width: 5px;"><!-- <input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"> --></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).' '. ucfirst($row["middlename"]).'</td>
                        <td>'. $row["course"].'-'. $row["level"].'</td>
                        ';
                        /*$log_type = "No QR IN";
                        $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                        if($res != "") {
                            echo date("h:i:s A", strtotime($res["time_enter"]));
                        } else {
                            $log_type = "Time-in";
                            $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                            if($res != "") {
                                echo date("h:i:s A", strtotime($res["time_enter"]));
                            } else {
                                echo '<button class="btn btn-success btn-sm exempt-student-btn timein-'. $row["stud_num"].'" data-event="'. $event_id.'"
                                data-log="Time-in" data-studid="'. $row["stud_num"].'">Time-in</button>';
                            }
                        }
                echo'</td>';*/

               /* echo'<td>';
                        $log_type = "No QR OUT";
                        $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                        if($res != "") {
                            echo date("h:i:s A", strtotime($res["time_enter"]));
                        } else {
                            $log_type = "Time-out";
                            $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                            if($res != "") {
                                echo date("h:i:s A", strtotime($res["time_enter"]));
                            } else {
                                echo '<button class="btn btn-danger btn-sm exempt-student-btn timeout-'. $row["stud_num"].'" data-event="'. $event_id.'"
                                        data-log="Time-out" data-studid="'. $row["stud_num"].'">Time-out</button>';
                            }
                        }
                echo '</td><td>';
                        $log_type = "Exempted";
                        $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                        if($res != "") {
                            echo date("h:i:s A", strtotime($res["time_enter"]));
                        } else {
                            echo '<button class="btn btn-info btn-sm exempt-student-btn exempted-'. $row["stud_num"].'" data-event="'. $event_id.'"
                                    data-log="exempted" data-studid="'. $row["stud_num"].'">Exempted</button>';
                        }
                echo '</td>
                    </tr>
                    ';*/
                echo '</tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewAllStudentList() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user ORDER BY lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }
                echo '
                    <tr class="payment-stud" style="cursor: pointer;" data-id="'. $row['stud_num']. '" data-fullname="'. $row["lastname"].", ". $row["firstname"]. '">
                        <td style="width: 5px;"><!-- <input type="checkbox" class="student_qr" value="0" data-id="'. $row["id"]. '"> --></td>
                        <td>'. $row["stud_num"].'</td>
                        <td><b>'. ucfirst($row["lastname"]).'</b>, '. ucfirst($row["firstname"]).'</td>
                        <td>'. $row["course"].'</td>
                        ';
                /*$log_type = "No QR IN";
                $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                if($res != "") {
                    echo date("h:i:s A", strtotime($res["time_enter"]));
                } else {
                    $log_type = "Time-in";
                    $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                    if($res != "") {
                        echo date("h:i:s A", strtotime($res["time_enter"]));
                    } else {
                        echo '<button class="btn btn-success btn-sm exempt-student-btn timein-'. $row["stud_num"].'" data-event="'. $event_id.'"
                        data-log="Time-in" data-studid="'. $row["stud_num"].'">Time-in</button>';
                    }
                }
        echo'</td>';*/

                /* echo'<td>';
                         $log_type = "No QR OUT";
                         $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                         if($res != "") {
                             echo date("h:i:s A", strtotime($res["time_enter"]));
                         } else {
                             $log_type = "Time-out";
                             $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                             if($res != "") {
                                 echo date("h:i:s A", strtotime($res["time_enter"]));
                             } else {
                                 echo '<button class="btn btn-danger btn-sm exempt-student-btn timeout-'. $row["stud_num"].'" data-event="'. $event_id.'"
                                         data-log="Time-out" data-studid="'. $row["stud_num"].'">Time-out</button>';
                             }
                         }
                 echo '</td><td>';
                         $log_type = "Exempted";
                         $res = $this->getStudentTimeInExemption($event_id, $log_type, $row["stud_num"]);
                         if($res != "") {
                             echo date("h:i:s A", strtotime($res["time_enter"]));
                         } else {
                             echo '<button class="btn btn-info btn-sm exempt-student-btn exempted-'. $row["stud_num"].'" data-event="'. $event_id.'"
                                     data-log="exempted" data-studid="'. $row["stud_num"].'">Exempted</button>';
                         }
                 echo '</td>
                     </tr>
                     ';*/
                echo '</tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getDataByStudentID($student_id) {
        try {
            $result[] = null;
            $this->openConnection();
            $id = explode(",", $student_id);
            $y = 0;
            foreach($id as $row) {
                if($row != null || $row != "") {
                    $data[] = null;
                    $sql = "SELECT * FROM tbl_user WHERE id = ?";
                    $stmt = $this->dbh->prepare($sql);
                    $stmt->bindParam(1, $row);
                    $stmt->execute();

                    $i = 0;
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $arr[] = null;
                        $arr["student_id"] = $row["stud_num"];
                        $arr["lastname"] = $row["lastname"];
                        $arr["firstname"] = $row["firstname"]. " ". $row["middlename"];
                        $arr["course"] = $row["course"];
                        $arr["college"] = $row["college"];
                        $arr["major"] = $row["major"];
                        $data[$i] = $arr;
                        $i++;
                    }
                    $result[$y] = $data;
                    $y++;
                }
            }

            $this->closeConnection();
            return $result;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getSingleDataByStudentID($student_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getTrackDataByCode($student_number) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["student_number"] = $row["stud_num"];
                $arr["image"] = $row["image"];
                $arr["id"] = $row["id"];
            }

            $this->closeConnection();

            return $arr;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getDataByCollege($college, $level) {
        try {
            $data[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ? AND level = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college);
            $stmt->bindParam(2, $level);
            $stmt->execute();

            $i = 0;
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr[] = null;
                $arr["student_id"] = $row["stud_num"];
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"]. " ". $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["major"] = $row["major"];
                $arr["college"] = $row["college"];
                $data[$i] = $arr;
                $i++;
            }

            $this->closeConnection();

            return $data;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getServerData() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_server WHERE id = 1";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getEventsOption() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_event WHERE type = 'University'";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<option value="'. $row["id"].'">'. $row["name"].' ('. date("F j, Y", strtotime($row["date"])).')</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addStudent($student_id, $lastname, $firstname, $middlename, $course, $major, $college, $image_name) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_user VALUES(0, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $lastname);
            $stmt->bindParam(3, $firstname);
            $stmt->bindParam(4, $middlename);
            $stmt->bindParam(5, $course);
            $stmt->bindParam(6, $major);
            $stmt->bindParam(7, $college);
            $stmt->bindParam(8, $image_name);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addEvent($name, $year, $semester, $fines, $auth_id, $date, $type, $fk_id) {
        try {
            $this->openConnection();

            $sql = " INSERT INTO tbl_event VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $year);
            $stmt->bindParam(3, $semester);
            $stmt->bindParam(4, $fines);
            $stmt->bindParam(5, $auth_id);
            $stmt->bindParam(6, $date);
            $stmt->bindParam(7, $type);
            $stmt->bindParam(8, $fk_id);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addExemptionByStudentID($student_id) {
        try {
            $result[] = null;
            $this->openConnection();
            $id = explode(",", $student_id);
            $y = 0;
            foreach($id as $row) {
                if($row != null || $row != "") {
                    $data[] = null;
                    $sql = "SELECT * FROM tbl_user WHERE id = ?";
                    $stmt = $this->dbh->prepare($sql);
                    $stmt->bindParam(1, $row);
                    $stmt->execute();

                    $i = 0;
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $arr[] = null;
                        $arr["student_id"] = $row["stud_num"];
                        $arr["lastname"] = $row["lastname"];
                        $arr["firstname"] = $row["firstname"];
                        $arr["course"] = $row["course"];
                        $data[$i] = $arr;
                        $i++;
                    }
                    $result[$y] = $data;
                    $y++;
                }
            }

            $this->closeConnection();
            return $result;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function addDepartment($name, $course_name, $dept_acro, $course_acro, $course_major, $college_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_department VALUES (0, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->bindParam(2, $name);
            $stmt->bindParam(3, $course_name);
            $stmt->bindParam(4, $dept_acro);
            $stmt->bindParam(5, $course_acro);
            $stmt->bindParam(6, $course_major);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewDepartment($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_department WHERE college_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <span class="detail-title">'. $row["name"].'</span>
                            <span style="display: block"><span class="fa fa-graduation-cap"></span> '. $row["course_name"].'</span>
                        </div>
                     </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addTimeLogOnExemption($event_id, $log_type, $student_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_time_log VALUES (0, ?, ?, ?, now())";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $event_id);
            $stmt->bindParam(3, $log_type);
            $stmt->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function addSemester($year, $semester) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_act_curr VALUES (0, ?, ?, 0)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $year);
            $stmt->bindParam(2, $semester);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewSemester() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr ORDER by year, semester";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $sem = "";
                if($row["semester"] == 1) {
                    $sem = "1st Semester";
                } elseif ($row["semester"] == 2) {
                    $sem = "2nd Semester";
                } elseif ($row["semester"] == 3) {
                    $sem = "Summer";
                }

                if($row["is_active"] == 0) {
                    $act = '<a href="../controllers/administrator/SetSemesterActive.php?id='. $row["id"]. '&year='. $row["year"]. '&semester='. $row["semester"]. '" class="btn btn-primary btn-xs">Set as Default</a>';
                } else {
                    $act = "Default";
                }
                echo '
                        <tr>
                            <td>'. $row["year"]. '-'. ($row["year"] + 1). '</td>
                            <td>'. $sem. '</td>
                            <td>'. $act. '</td>
                        </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function setAsActiveSemester($id) {
        try {
            $this->openConnection();

            $sql1 = "UPDATE tbl_act_curr SET is_active = 0";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->execute();

            $sql = "UPDATE tbl_act_curr SET is_active = 1 WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getSchoolYearOption() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr ORDER BY year, semester DESC";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $sem = "";
                if($row["semester"] == 1) {
                    $sem = "1st Semester";
                } elseif ($row["semester"] == 2) {
                    $sem = "2nd Semester";
                } elseif ($row["semester"] == 3) {
                    $sem = "Summer";
                }
                echo '<option value="'. $row["id"]. '">'. $row["year"]. '-'. ($row["year"] + 1). ' '. $sem. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getActiveSemester($id) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["year"] = $row["year"];
                $arr["semester"] = $row["semester"];
            }

            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addPayment($name, $description, $amount, $year, $semester, $auth_id, $type, $fk_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_payment VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $name);
            $stmt->bindParam(2, $description);
            $stmt->bindParam(3, $amount);
            $stmt->bindParam(4, $year);
            $stmt->bindParam(5, $semester);
            $stmt->bindParam(6, $auth_id);
            $stmt->bindParam(7, $type);
            $stmt->bindParam(8, $fk_id);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewPayment($type, $year, $semester) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment WHERE type = ? AND year = ? AND semester = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $type);
            $stmt->bindParam(2, $year);
            $stmt->bindParam(3, $semester);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<div class="col-md-4">
                        <div class="well detail-list">
                            <b>'. $row["name"]. '</b> <br/>
                            <span style="display: block">Amount: '. $row["amount"]. '</span>
                            <a href="operate_payment.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">Collect By QR</a>
                            <a href="manual_payment.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">Manual Collect</a>
                            <a href="generate_payment_report.php?payment_id='. $row["id"]. '" class="btn btn-primary btn-xs">Generate Report</a>
                            <a href="#" class="btn btn-danger btn-xs">Remove</a>
                        </div>
                    </div>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentData($payment_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function checkIfExistPayment($stud_num, $payment_id, $type) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_payment_log WHERE fk_id = ? AND type = ? AND student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->bindParam(2, $type);
            $stmt->bindParam(3, $stud_num);
            $stmt->execute();
            $i = false;
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $i = true;
            }

            $this->closeConnection();

            return $i;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addPaymentLog($stud_num, $or_number, $total_pay, $amt_pay, $amt_tender, $type, $payment_id) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_payment_log VALUES(0, ?, ?, ?, ?, ?, now(), ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $stud_num);
            $stmt->bindParam(2, $or_number);
            $stmt->bindParam(3, $total_pay);
            $stmt->bindParam(4, $amt_pay);
            $stmt->bindParam(5, $amt_tender);
            $stmt->bindParam(6, $type);
            $stmt->bindParam(7, $payment_id);
            $stmt->execute();

            $sql1 = "SELECT * FROM tbl_payment_log WHERE student_id = ? AND type = ? AND fk_id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $stud_num);
            $stmt1->bindParam(2, $type);
            $stmt1->bindParam(3, $payment_id);
            $stmt1->execute();

            $res = $stmt1->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res["id"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentLogID($stud_num, $type, $payment_id) {
        try {
            $this->openConnection();

            $sql1 = "SELECT * FROM tbl_payment_log WHERE student_id = ? AND type = ? AND fk_id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $stud_num);
            $stmt1->bindParam(2, $type);
            $stmt1->bindParam(3, $payment_id);
            $stmt1->execute();

            $res = $stmt1->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res["id"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentLog($id) {
        try {
            $this->openConnection();

            $sql1 = "SELECT * FROM tbl_payment_log WHERE id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $id);
            $stmt1->execute();

            $res = $stmt1->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getPaymentLogByPaymentID($payment_id, $type) {

        try {
            $this->openConnection();

            $sql1 = "SELECT p.id AS p_id, p.student_id, u.lastname, u.firstname, u.course, u.major, u.college, p.or_number, p.total_pay, p.date_pay
                     FROM tbl_payment_log p, tbl_user u WHERE p.student_id = u.stud_num AND p.fk_id = ? AND p.type = ?
                     ORDER BY p.id DESC";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $payment_id);
            $stmt1->bindParam(2, $type);
            $stmt1->execute();

            while($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }
                echo '<tr class="payment-'. $row["p_id"]. '">
                          <td>'. $row["student_id"]. '</td>
                          <td>'. $row["lastname"]. ', '. $row["firstname"]. '</td>
                          <td>'. $row["course"]. '</td>
                          <td>Paid</td>
                          <td>'. date('F d, Y', strtotime($row["date_pay"])).'</td>
                          <td>
                              <a class="btn btn-primary btn-xs" target="_blank" href="print_pay_log.php?id='. $row["p_id"]. '"><i class="fa fa-print"></i> Re-print O.R.</a>
                              <button class="btn btn-danger btn-xs remove-pay-log" data-id="'. $row["p_id"]. '"><i class="fa fa-trash-o"></i> Remove</button>
                          </td>
                      </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function deletePaymentLogByID($pay_log_id) {
        try {
            $this->openConnection();

            $sql1 = "DELETE FROM tbl_payment_log WHERE id = ?";
            $stmt1 = $this->dbh->prepare($sql1);
            $stmt1->bindParam(1, $pay_log_id);
            $stmt1->execute();

            $this->closeConnection();
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function checkTimeLogJSON($student_id, $log_type, $event_id) {
        try {
            $this->openConnection();

            $sql = "SELECT time_enter FROM tbl_time_log WHERE student_id = ? AND status = ? AND event_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $log_type);
            $stmt->bindParam(3, $event_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res["time_enter"];
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function viewCollegeSelection() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_college ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<option value="'. $row["id"]. '">'. $row["name"]. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function viewDepartmentSelection($college_id) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_department WHERE college_id = ? ORDER BY name";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_id);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo '<option value="'. $row["id"]. '">'. $row["name"]. '</option>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addAccount($student_id, $password, $org_type, $fk_id, $status) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_auth_user VALUES(0, ?, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $password);
            $stmt->bindParam(3, $org_type);
            $stmt->bindParam(4, $fk_id);
            $stmt->bindParam(5, $status);
            $stmt->execute();

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getOrganizationName($type, $fk_id) {
        try {
            $this->openConnection();

            $sql = "";

            if($type == "College") {
                $sql = "SELECT name FROM tbl_college WHERE id = ?";
            } elseif($type == "Department") {
                $sql = "SELECT name FROM tbl_department WHERE id = ?";
            }
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $fk_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            if($type != "University") {
                return $res["name"];
            } else {
                return "University Account";
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getUserAccounts() {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_auth_user";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $auth_type = "";

                if($row["status"] == 0) {
                    $auth_type = "Administrator";
                } else {
                    $auth_type = "Encoder";
                }
                echo '<tr>
                        <td>'. $row["student_id"]. '</td>
                        <td>'. $this->getOrganizationName($row["type"], $row["fk_id"]). '</td>
                        <td>'. $row["type"]. ' - '. $auth_type. '</td>
                        <td>
                            <a href="#" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i> Edit Roles</a>
                            <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Remove</a>
                        </td>
                    </tr>';
            }

            $this->closeConnection();
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    

    function getAccountLastName($student_id) {
        try {
            $this->openConnection();
            $sql = "SELECT lastname FROM tbl_user WHERE stud_num = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->closeConnection();
            return $res["lastname"];
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getStudentTotalPaidByPaymentID($student_id, $payment_id, $type) {
        try {
            $total_paid = 0;
            $this->openConnection();

            $sql = "SELECT total_pay FROM tbl_payment_log WHERE fk_id = ? AND student_id = ? AND type = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $payment_id);
            $stmt->bindParam(2, $student_id);
            $stmt->bindParam(3, $type);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $total_paid += $row["total_pay"];
            }


            $this->closeConnection();

            return $total_paid;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function displayAllPaidPaymentByID($payment_id, $fee, $college_acro, $type) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_user WHERE college = ? ORDER BY course, major, lastname";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $college_acro);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                if($row["major"] != " ") {
                    $row["course"] = $row["course"] . $row["major"];
                }

                $total_paid = $this->getStudentTotalPaidByPaymentID($row["stud_num"], $payment_id, $type);

                echo '<tr>
                            <td>'. $row["stud_num"]. '</td>
                            <td>'. $row["lastname"]. ', '. $row["firstname"]. '</td>
                            <td>'. $row["course"]. '</td>
                            <td>P '. $total_paid. '.00</td>
                            <td></td>
                        </tr>';
            }
            $this->closeConnection();

        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}