<?php


include "ConnectionDAO.php";

class ServerDAO extends ConnectionDAO {
    function editServer($id, $host, $username, $password) {
        try {
            $this->openConnection();

            $sql = "UPDATE tbl_server SET host = ?, username = ?, password = ? WHERE id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $host);
            $stmt->bindParam(2, $username);
            $stmt->bindParam(3, $password);
            $stmt->bindParam(4, $id);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addData($student_number, $lastname, $firstname, $middlename, $course, $major, $college, $level) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_user VALUES(0, ?, ?, ?, ?, ?, ?, ?, 'default.png', ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->bindParam(2, $lastname);
            $stmt->bindParam(3, $firstname);
            $stmt->bindParam(4, $middlename);
            $stmt->bindParam(5, $course);
            $stmt->bindParam(6, $major);
            $stmt->bindParam(7, $college);
            $stmt->bindParam(8, $level);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function addTimeLogData($student_id, $event_id, $status, $time_log) {
        try {
            $this->openConnection();

            $sql = "INSERT INTO tbl_time_log VALUES(0, ?, ?, ?, ?)";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $event_id);
            $stmt->bindParam(3, $status);
            $stmt->bindParam(4, $time_log);
            $stmt->execute();

            $this->closeConnection();

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}