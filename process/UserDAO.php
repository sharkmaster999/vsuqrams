<?php

include "ConnectionDAO.php";

class UserDAO extends ConnectionDAO {
    function loginUser($student_id, $password) {
        try {
            $this->openConnection();

            $sql = "SELECT * FROM tbl_auth_user WHERE student_id = ? AND password = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_id);
            $stmt->bindParam(2, $password);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->closeConnection();

            return $res;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    function getTrackDataByCode($student_number) {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_auth_user WHERE student_id = ?";
            $stmt = $this->dbh->prepare($sql);
            $stmt->bindParam(1, $student_number);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["lastname"] = $row["lastname"];
                $arr["firstname"] = $row["firstname"];
                $arr["middlename"] = $row["middlename"];
                $arr["course"] = $row["course"];
                $arr["student_number"] = $row["stud_num"];
                $arr["image"] = $row["image"];
                $arr["auth_type"] = $row["status"];
            }

            $this->closeConnection();

            return $arr;
        } catch(Exception $e) {
            $e->getMessage();
        }
    }

    function getActiveSemester() {
        try {
            $arr[] = null;
            $this->openConnection();

            $sql = "SELECT * FROM tbl_act_curr WHERE is_active = 1";
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $arr["year"] = $row["year"];
                $arr["semester"] = $row["semester"];
            }

            $this->closeConnection();

            return $arr;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}