<?php
include "../process/AdministratorDAO.php";

$process = new AdministratorDAO();

$data = $process->getPaymentData($_GET["payment_id"]);
$college_data = $process->getCollegeDataByAcronym($_GET["college_acro"]);
?>
<div>
    <!--Visayas State University <br/>
    University Supreme Student Council (VSU-USSC)<br/>
    <?php /*echo $college_data["name"]*/?> - Supreme Student Council (<?php /*echo $_GET["college_acro"]*/?>-SSC)
    <br/><br/>
    <?php /*echo $data["name"]*/?> (P <?php /*echo $data["amount"]*/?>) <br/><br/>-->
    <table class="report-list">
        <thead>
        <tr>
            <th style="width: 15%;">Student No.</th>
            <th style="width: 40%">Name</th>
            <th style="width: 30%">Course</th>
            <th style="width: 40%">Total Paid</th>
            <th style="width: 20%;">Remarks</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $process->displayAllPaidPaymentByID($data["id"], $data["amount"], $_GET["college_acro"], $_GET["type"]);
        ?>
        </tbody>
    </table>
</div>