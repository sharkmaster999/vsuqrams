<?php include "header.php";?>
<?php include "navigation.php";?>
<?php

$server =  $process->getServerData();

?>
<div class="container">
    <?php
    if(isset($_GET["success"])) {
        echo "<br>";
        if($_GET["success"] == 1) {
            echo '<div class="alert alert-info">
                                  <span class="glyphicon glyphicon-info-sign"></span> Data successfully imported!
                                  <span class="close" data-dismiss="alert">&times;</span>
                              </div>';
        } else if($_GET["success"] == 2) {
            echo '<div class="alert alert-info">
                                  <span class="glyphicon glyphicon-info-sign"></span> Server to sync in successfully updated!
                                  <span class="close" data-dismiss="alert">&times;</span>
                              </div>';
        }
    }
    ?>
    <h2>Data Management</h2>
    <a href="#exportTimeLog" class="btn btn-primary btn-sm" data-toggle="modal"><span class="glyphicon glyphicon-time"></span> Export Time Log</a>
    <a href="#importData" class="btn btn-primary btn-sm" data-toggle="modal"><span class="glyphicon glyphicon-floppy-save"></span> Import Data</a>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <div class="well" style="width: 100%;">
                <b>Server Sync: </b> <br>
                <span style="font-size: 18px;">Host: <?php echo $server["host"]?></span> <br>
                Username: <?php echo $server["username"]?> <br>
                Password: <?php echo $server["password"]?><br>
                <a href="#editServer" class="btn btn-primary btn-xs" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span> Edit Server</a>
            </div>
        </div>
        <div class="col-md-8">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Server Log</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editServer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Server</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/EditServer.php" method="POST" class="server-frm">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="#">Host: </label>
                                <input type="hidden" name="server_id" value="<?php echo $server["id"]?>"/>
                                <input type="text" name="host" class="form-control" autocomplete="off" value="<?php echo $server["host"]?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="#">Username: </label>
                                <input type="text" name="username" class="form-control" autocomplete="off" value="<?php echo $server["username"]?>"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="#">Password: </label>
                                <input type="password" name="password" class="form-control" autocomplete="off" value="<?php echo $server["password"]?>"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary edit-server-btn"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="importData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Import Data</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/ImportData.php" method="POST" class="import-frm"enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="#">Type of file: </label>
                                <select name="file_type" id="" class="form-control">
                                    <option value="Students">Student Data</option>
                                    <option value="Time">Time Log</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="#">File to Import: </label>
                                <input type="file" name="excel_file" class="form-control" autocomplete="off" value="<?php echo $server["username"]?>"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary import-btn"><span class="glyphicon glyphicon-floppy-save"></span> Import</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exportTimeLog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Export Time Log</h4>
            </div>
            <div class="modal-body">
                <form action="export_time_log.php" method="POST" class="export-time-frm">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="#">Events: </label>
                                <select name="event_id" id="" class="form-control">
                                    <?php
                                    $process->getEventsOption();
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary export-time-btn"><span class="glyphicon glyphicon-export"></span> Export</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../js/source.js"></script>
</body>
</html>