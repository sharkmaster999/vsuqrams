<?php include "header.php";?>
<?php include "navigation.php";?>

<?php


$data = $process->getPaymentData($_GET["payment_id"]);
?>

<div class="container">
    <h5>Visayas State University</h5>
    <h5>University Supreme Student Council</h5>
    <h5><?php echo $data["name"]?> (P <?php echo $data["amount"]?>)</h5>
    <input type="hidden" name="payment_id" id="payment_id" value="<?php echo $data["id"]?>"/>
    <input type="hidden" name="type" id="type" value="University"/>
    <button class="print-report btn btn-primary btn-sm"><i class="fa fa-print"></i> Print</button>
    <button class="excel-report btn btn-primary btn-sm"><i class="fa fa-file-excel-o"></i> Excel Form</button>
    <hr/>
    <div class="row">
        <div class="col-md-4">
            <select name="selectCollege" id="selectCollege" class="form-control">
                <option>--- Select College to View ---</option>
                <option value="CoE">College of Engineering</option>
                <option value="CFES">College of Forestry and Environmental Science</option>
                <option value="CAFS">College of Agriculture and Food Science</option>
                <option value="CAS">College of Arts and Sciences</option>
                <option value="CME">College of Management and Economics</option>
                <option value="CVM">College of Veterinary Medicine</option>
                <option value="CE">College of Education</option>
                <option value="CN">College of Nursing</option>
            </select>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12" id="plog-list">

        </div>
    </div>
</div>

<div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Set Semester</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/ActiveSemester.php" method="POST" class="semester1-frm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="#">School Year: </label>
                                <select name="act_curr_id" id="" class="form-control">
                                    <?php
                                    $process->getSchoolYearOption();
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.js"></script>
<script type="text/javascript" src="../js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="../js/administrator.js"></script>
<script type="text/javascript">
    $(function () {
        $(document).on("change", "#selectCollege", function () {
            var payment_id = $("#payment_id").val();
            var type = $("#type").val();
            var college_acro = $(this).val();

            $("#plog-list").html("<p>Loading data. Please wait...</p>");

            getPaymentLogByPaymentID(payment_id, college_acro, type);
            document.getElementById("plog-list").print();
        });

        $(".print-report").on("click", function () {
            $(this).hide();
            $(".excel-report").hide();
            window.print();
            $(this).show();
            $(".excel-report").show();
        });

    });

    function getPaymentLogByPaymentID(payment_id, college_acro, type) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("plog-list").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open ("GET","payment_record.php?payment_id="+payment_id+"&college_acro="+college_acro+"&type="+type+"", true);
        xmlhttp.send();
    }

</script>
</body>
</html>