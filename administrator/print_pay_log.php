<?php
    include "../process/AdministratorDAO.php";

    $process = new AdministratorDAO();

    $log_data = $process->getPaymentLog($_GET["id"]);
    $payment_data = $process->getPaymentData($log_data["fk_id"]);
    $student_data = $process->getSingleDataByStudentID($log_data["student_id"]);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Print Payment Log</title>
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
        <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
        <link rel="stylesheet" href="../css/font-awesome.min.css"/>
        <link href="../css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="" media="print"/>
    </head>
    <body>
        <div style="border: 1px solid #000000; width: 4.05in;">
            <!--<div class="text-center">
                <b>Visayas State University</b><br/>
                <b>University Supreme Student Council</b>
            </div>-->
            <h3 class="text-center">R E C E I P T</h3>
            <div style="margin-left: 15px;">
                <span>O.R. Number: <b><?php echo $log_data["or_number"]?></b></span> <br/>
                <span>Student: <b><?php echo $log_data["student_id"]?> <?php echo $student_data["lastname"]?>,  <?php echo $student_data["firstname"]?></b></span> <br/>
                <span>Payment for: <b><?php echo $payment_data["name"]?> &nbsp;&nbsp;&nbsp;</b><br/> Amount: <b>P <?php echo $payment_data["amount"]?> </b></span> <br/>
            </div>
            <br/><br/>
            <div style="border-top: 1px solid #000000; width: 250px; text-align: center; margin: 0 auto;">
                <span class="text-center">Authorized Signature</span>
            </div>
        </div>
        <script type="text/javascript">
            window.print();
        </script>
    </body>
</html>