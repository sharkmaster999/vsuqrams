<?php include "header.php";?>
<?php include "navigation.php";?>

<div class="container">
    <?php
        if(isset($_SESSION["success"])) {
            echo "<br><div class='alert alert-success'>". $_SESSION['success']."</div>";
            unset($_SESSION["success"]);
        }
    ?>
    <h3><i class="fa fa-cog"></i> Settings</h3>
    <hr/>
    <div class="panel panel-info">
        <div class="panel-heading">
            <b>Active Semesters <a href="#addSemester" data-toggle="modal" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Add Semester</a></b>
        </div>
        <div class="panel-body" style="padding-bottom: 0;">
            <table class="table table-bordered table-hover student-list">
                <thead>
                <tr>
                    <th>School Year</th>
                    <th>Semester</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $process->viewSemester();
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="addSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Add New School Year and Semester</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/AddSemester.php" method="POST" class="semester-frm">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="#">School Year Start: </label>
                                <input type="text" name="year" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="#">Semester: </label>
                                <select name="semester" id="" class="form-control">
                                    <option value="">Select One</option>
                                    <option value="1">1st Semester</option>
                                    <option value="2">2nd Semester</option>
                                    <option value="3">Summer</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-semester-btn"><span class="fa fa-plus"></span> Add Semester</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<?php include "footer.php";?>
