<?php include "header.php";?>
<?php include "navigation.php";?>


<div class="container">
    <h2>Colleges</h2>
    <?php include "active_sem.php";?>

    <hr>
    <div class="row">
        <?php

            include_once "../process/AdministratorDAO.php";

            $process = new AdministratorDAO();

            $process->viewCollege();

        ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Add College</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/AddCollege.php" method="POST" class="college-frm">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="#">Name: </label>
                                <input type="text" name="name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#">Acronym: </label>
                                <input type="text" name="acronym" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary add-college-btn"><span class="glyphicon glyphicon-plus"></span> Add College</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php";?>
