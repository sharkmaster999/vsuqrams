<?php include "header.php";?>
<?php include "navigation.php";?>

<div class="container">
    <?php
    if(isset($_SESSION["success"])) {
        echo "<br><div class='alert alert-success'>". $_SESSION['success']."</div>";
        unset($_SESSION["success"]);
    }
    ?>
    <h2>Payments</h2>
    <?php include "active_sem.php"?>
    <a href="#addPayment" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-money"></i> Add Payment</a>
    <hr>
    <div class="row">
        <?php
            $process->viewPayment("University", $_SESSION["year"], $_SESSION["semester"]);
        ?>
    </div>

</div>
<!-- Modal -->
<div class="modal" id="addPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <b class="modal-title" id="myModalLabel">Add Payment</b>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/AddPayment.php" method="POST" class="payment-frm">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="#">Name: </label>
                                <input type="hidden" name="year" value="<?php echo $_SESSION["year"]?>"/>
                                <input type="hidden" name="semester" value="<?php echo $_SESSION["semester"]?>"/>
                                <input type="hidden" name="auth_id" value="<?php echo $_SESSION["student_id"]?>"/>
                                <input type="hidden" name="type" value="<?php echo $_SESSION["type"]?>"/>
                                <input type="hidden" name="fk_id" value="<?php echo $_SESSION["fk_id"]?>"/>
                                <input type="text" name="name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="#">Amount: </label>
                                <input type="text" name="amount" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="#">Description: </label>
                                <input type="text" name="description" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm add-payment-btn"><span class="fa fa-money"></span> Add Payment</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php";?>
