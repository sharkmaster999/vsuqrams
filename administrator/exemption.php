<?php
include "../process/AdministratorDAO.php";
session_start();
$process = new AdministratorDAO();

$data = $process->getEventData($_GET["event_id"]);
$college_data = $process->getCollegeData($data["fk_id"]);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | Administrator</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-4">
            <h3>Exemption</h3>
            <h4><?php echo ucwords($data["name"])?> (<?php echo ucwords($college_data["acronym"])?>)</h4>
            <?php include "active_sem.php";?>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <input type="hidden" class="event_id_fld" name="event_id" value="<?php echo $_GET["event_id"];?>"/>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <!--<form action="../controllers/administrator/AddExemption.php" method="POST" class="exemption_frm">
                        <input type="hidden" name="noqr_stud" class="form-control noqr_stud"/>
                        <input type="hidden" name="exemption" class="exempt"/>
                        <input type="hidden" name="event_id" value="<?php /*echo $_GET["event_id"];*/?>"/>
                        <button class="btn btn-primary btn-xs exemption_btn" style="float: right" data-content="Exempted"><span class="fa fa-plus"></span> Add as Exempted</button>
                        <button class="btn btn-primary btn-xs exemption_btn" style="float: right" data-content="No QR OUT"><span class="fa fa-plus"></span> Add as No QR for OUT</button>
                        <button class="btn btn-primary btn-xs exemption_btn" style="float: right" data-content="No QR IN"><span class="fa fa-plus"></span> Add as No QR for IN</button>
                    </form>-->

                    <!-- <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control" placeholder="Search name to confirm"/>
                                <div class="">
                                    <span class="fa fa-search"></span> Search
                                </div>
                            </div>
                            <div class="well">
                                asdasd
                            </div>
                        </div>
                    </div> -->
                    <table class="table table-bordered student-list table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Student ID</th>
                            <th>Names</th>
                            <th>Course</th>
                            <!--<th style="width: 50px;">Time-in</th>
                            <th style="width: 50px;">Time-out</th>
                            <th style="width: 50px;">Exempted</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        
                           <?php
                                $process->viewStudentList($college_data["acronym"]);
                           ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Register Student</h3>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/AddStudent.php" method="POST" class="student-frm">
                        <p >Please fill-up all the fields to register.</p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Last Name: </label>
                                    <input type="text" name="lastname" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">First Name: </label>
                                    <input type="text" name="firstname" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Middle Name: </label>
                                    <input type="text" name="middlename" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Student ID: </label>
                                    <input type="text" name="student_id" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="#">Course: </label>
                                    <select name="course" id="" class="form-control">
                                        <option>--- Select Course ---</option>
                                        <option value="BSCE">Bachelor of Science in Civil Engineering</option>
                                        <option value="BSGE">Bachelor of Science in Geodetic Engineering</option>
                                        <option value="BSAE">Bachelor of Science in Agricultural Engineering</option>
                                        <option value="BSME">Bachelor of Science in Mechanical Engineering</option>
                                        <option value="BSCS">Bachelor of Science in Computer Science</option>
                                        <option value="DVM">Doctor of Veterinary Medicine</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary add-student-btn"><span class="glyphicon glyphicon-user"></span> Add Student</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="generateQR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Generate QR Code</h3>
                </div>
                <div class="modal-body">
                    <form action="generate_qr.php" method="POST" class="generateqr-frm">
                        <p >Please choose what course you want to generate code</p>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="#">Course: </label>
                                    <select name="course" id="" class="form-control">
                                        <option>--- Select Course ---</option>
                                        <option value="BSCE">Bachelor of Science in Civil Engineering</option>
                                        <option value="BSGE">Bachelor of Science in Geodetic Engineering</option>
                                        <option value="BSAE">Bachelor of Science in Agricultural Engineering</option>
                                        <option value="BSME">Bachelor of Science in Mechanical Engineering</option>
                                        <option value="BSCS">Bachelor of Science in Computer Science</option>
                                        <option value="DVM">Doctor of Veterinary Medicine</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary generateqr-btn"><span class="glyphicon glyphicon-user"></span> Generate</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal" id="addExemption" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b style="font-size: 12px;" class="modal-title ex-title" id="myModalLabel"></b>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" style="font-size: 12px;">
                        <tr>
                            <th>Time-in</th>
                            <th>Time-out</th>
                        </tr>
                        <tr>
                            <td class="disp-t1"><button class="btn btn-primary btn-xs">Time in now</button></td>
                            <td class="disp-t2"><button class="btn btn-primary btn-xs">Time out now</button></td>
                        </tr>
                    </table>
                </div>
                <!--<div class="modal-footer">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="activeSemester" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <b class="modal-title" id="myModalLabel">Set Semester</b>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/ActiveSemester.php" method="POST" class="semester1-frm">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="#">School Year: </label>
                                    <select name="act_curr_id" id="" class="form-control">
                                        <?php
                                        $process->getSchoolYearOption();
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm add-semester1-btn"><span class="fa fa-check"></span> Set as Active</button>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>
    <script type="text/javascript">
        $(function () {

            $(".add-semester1-btn").on("click", function () {
                $(".semester1-frm").submit();
            });

        })
    </script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>