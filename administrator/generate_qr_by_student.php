<!DOCTYPE html>
<html>
<head>
    <title>Print QR Code</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
</head>
<body>
<?php

include "../phpqrcode/qrlib.php";
include "../process/AdministratorDAO.php";

$stud_ids = $_POST["noqr_stud"];
$process = new AdministratorDAO();

$data = $process->getDataByStudentID($stud_ids);

foreach($data as $row1) {
    foreach ($row1 as $row) {
        if($row["lastname"] != null) {
            if($row["college"] == "COE") {
                $college = "College of Engineering";
            } else if($row["college"] == "CFES") {
                $college = "College of Forestry and Environmental Science";
            } else if($row["college"] == "CE") {
                $college = "College of Education";
            } else if($row["college"] == "CME") {
                $college = "College of Management and Economics";
            } else if($row["college"] == "CAFS") {
                $college = "College of Agriculture and Food Science";
            } else if($row["college"] == "CAS") {
                $college = "College of Arts and Science";
            } else if($row["college"] == "CN") {
                $college = "College of Nursing";
            } else if($row["college"] == "CVM") {
                $college = "College of Veterinary Medicine";
            } else {
                $college = "College of IDK";
            }
            $bg_color = "background-color: #fff !important;";
            $f_color = "color: #000 !important;";
            $img = "sign-black.png";

            if($row["major"] != " ") {
                $row["course"] = $row["course"] . " " . $row["major"];
            }

            //}
            echo "<div style='border: 1px solid #000; ". $bg_color. " color: #fff !important; padding: 2px; width: 188px; float: left; margin:  5px 5px 10px 5px; line-height: 1.2;' class='text-center'>
                  <br>
                  <span style='font-size: 10px; margin-top: 10px;". $f_color. "'>Visayas State University</span><br>
                  <span style='font-size: 9px; ". $f_color. "'>University Supreme Student Council</span><br>
                  <span style='font-size: 8px; ". $f_color. "'>". $college. "</span><br>
                  <span style='font-size: 7px; font-weight: bold;". $f_color. "'>". $row["lastname"]. ", ". $row["firstname"]. " - ". $row["course"]. "</span><br><br>";
            //set it to writable location, a place for temp generated PNG files
            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;

            //html PNG location prefix
            $PNG_WEB_DIR = 'temp/';

            //ofcourse we need rights to create temp dir
            if (!file_exists($PNG_TEMP_DIR))
                mkdir($PNG_TEMP_DIR);
            $errorCorrectionLevel = 'L';
            $matrixPointSize = 7;
            $filename = $PNG_TEMP_DIR.'test'.md5($row["student_id"]).'.png';
            QRcode::png($row["student_id"], $filename, $errorCorrectionLevel, $matrixPointSize, 2);

            echo '<img style="margin-top: 5px;" src="'.$PNG_WEB_DIR.basename($filename).'" />';
            echo "<br><br>
                  <img src='../img/". $img."' style='height: 42px'/><br>
                  <div style='font-size: 7px; line-height: 0.9; margin-top: 2px;". $f_color. "'>
                        <span>Authorized Signature</span><br><span style='font-size: 3px !important; '>&copy; VSU USSC-QRAMS. Created by Remuelito</span>
                  </div>
                </div>";
        }
    }
}

?>
</body>
</html>