<?php
include "../process/AdministratorDAO.php";

$process = new AdministratorDAO();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Visayas State University QR Code Attendance Monitoring System | Administrator</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-customize.css"/>
    <link rel="stylesheet" href="../css/dataTables.bootstrap.css"/>
    <link rel="stylesheet" href="../css/font-awesome.min.css"/>
    <link href="../css/style.css" rel="stylesheet">
</head>
<body>
<?php include "navigation.php";?>
<div class="container" id="QR-Code">
    <div class="row">
        <div class="col-md-4">
            <h3>Visayas State University <br>University Supreme Student Council</h3>
            <h4>Student Record Tracker</h4>
            <input type="hidden" id="Status" name="status" value="Time-in"/>
            <br>
        </div>
        <div class="col-md-8">
            <div style="float: right; margin-top: 3px;">
                <div class="form-group">
                    <select class="form-control" id="camera-select" style="display: none;"></select>
                    <br><br><br>
                    <!--<button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>-->
                    <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span> Capture</button>
                    <a href="#addStudent" class="btn btn-primary btn-sm" data-toggle="modal"><span class="glyphicon glyphicon-plus"></span> Register</a>
                    <a href="#generateQR" class="btn btn-primary btn-sm" data-toggle="modal"><span class="glyphicon glyphicon-qrcode"></span> Generate QR Code</a>
                    <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-play"></span> Start</button>
                    <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span> Pause</button>
                    <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span> Stop</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="well" style="width: 100%;">
                <canvas style="width: 100%;" id="webcodecam-canvas"></canvas>
                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="data-wrapper" style="width: 100%">
                <div class="image-storage" style="float: left;">

                </div>
                <div id="studentData">
                    <div style="border: 5px dashed #cccccc; text-align: center; padding: 20px; height: 224px;">
                        <br><br><br>
                        <h4 style="vertical-align: middle; color: #cccccc">Student information will display here if the system scan completely and has a data.</h4>
                    </div>
                </div>
            </div>
            <br>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                    <h3 class="modal-title" id="myModalLabel">Register Student</h3>
                </div>
                <div class="modal-body">
                    <form action="../controllers/administrator/AddStudent.php" method="POST" class="student-frm">
                        <p >Please fill-up all the fields to register.</p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Last Name: </label>
                                    <input type="text" name="lastname" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">First Name: </label>
                                    <input type="text" name="firstname" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Middle Name: </label>
                                    <input type="text" name="middlename" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="#">Student ID: </label>
                                    <input type="text" name="student_id" class="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="#">Course: </label>
                                    <select name="course" id="" class="form-control">
                                        <option>--- Select Course ---</option>
                                        <option value="BSCE">Bachelor of Science in Civil Engineering</option>
                                        <option value="BSGE">Bachelor of Science in Geodetic Engineering</option>
                                        <option value="BSAE">Bachelor of Science in Agricultural Engineering</option>
                                        <option value="BSME">Bachelor of Science in Mechanical Engineering</option>
                                        <option value="BSCS">Bachelor of Science in Computer Science</option>
                                        <option value="DVM">Doctor of Veterinary Medicine</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary add-student-btn"><span class="glyphicon glyphicon-user"></span> Add Student</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="generateQR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title" id="myModalLabel">Generate QR Code</h3>
                </div>
                <div class="modal-body">
                    <form action="generate_qr.php" method="POST" class="generateqr-frm">
                        <p >Please choose what course you want to generate code</p>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="#">Course: </label>
                                    <select name="course" id="" class="form-control">
                                        <option>--- Select Course ---</option>
                                        <option value="BSCE">Bachelor of Science in Civil Engineering</option>
                                        <option value="BSGE">Bachelor of Science in Geodetic Engineering</option>
                                        <option value="BSAE">Bachelor of Science in Agricultural Engineering</option>
                                        <option value="BSME">Bachelor of Science in Mechanical Engineering</option>
                                        <option value="BSCS">Bachelor of Science in Computer Science</option>
                                        <option value="DVM">Doctor of Veterinary Medicine</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary generateqr-btn"><span class="glyphicon glyphicon-user"></span> Generate</button>
                    <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/filereader.js"></script>
    <!-- Using jquery version: -->

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="../js/qrcodelib.js"></script>
    <script type="text/javascript" src="../js/webcodecamjquery.js"></script>
    <script type="text/javascript" src="../js/mainjquery.js"></script>
    <script type="text/javascript" src="../js/source.js"></script>

    <!-- <script type="text/javascript" src="js/qrcodelib.js"></script>
    <script type="text/javascript" src="js/webcodecamjs.js"></script>
    <script type="text/javascript" src="js/main.js"></script> -->
</body>
</html>