<?php include "header.php";?>
<?php include "navigation.php";?>
<?php
$college_data = $process->getCollegeData($_GET["id"]);
?>


<div class="container">
    <h3><?php echo ucwords($college_data["name"]) ." (" .strtoupper($college_data["acronym"]) . ")";?></h3>
    <?php include "active_sem.php";?>
    <a href="#addEvent" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Create Event</a>
    <hr>
    <div class="">
        <div class="row">
            <?php
                $process->viewCollegeEvent($_GET["id"], $_SESSION["semester"], $_SESSION["year"]);
            ?>
        </div>
    </div>
</div>

<div class="modal fade" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Create New Event</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/AddEvent.php" method="POST" class="event-frm" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name: </label>
                                <input type="hidden" id="type" name="type" value="College"/>
                                <input type="hidden" id="fk_id" name="fk_id" value="<?php echo $_GET["id"]?>"/>
                                <input type="hidden" name="year" value="<?php echo $_SESSION["year"]?>"/>
                                <input type="hidden" name="semester" value="<?php echo $_SESSION["semester"]?>"/>
                                <input type="text" id="name" name="name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date">Date: </label>
                                <input type="text" id="date" name="date" class="form-control event-date" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fines">Fines: </label>
                                <input type="text" id="fines" name="fines" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="auth_stud_id">Authorized Student ID #: </label>
                                <input type="text" id="auth_stud_id" name="auth_id" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm add-event-btn"><i class="fa fa-plus"></i> Create</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addDepartment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Department</h4>
            </div>
            <div class="modal-body">
                <form action="../controllers/administrator/AddDepartment.php" method="POST" class="dept-frm" enctype="multipart/form-data">
                    <p>Write the appropriate information. <span style="color: #ff0000; font-weight: bold">*</span></p>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="name">Department Name: </label>
                                <input type="hidden" id="college_id" name="college_id" value="<?php echo $_GET["id"]?>"/>
                                <input type="text" id="name" name="name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="dept_acro">Acronym: <span style="color: #ff0000">*</span></label>
                                <input type="text" id="dept_acro" name="dept_acro" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="course_name">Course Name: </label>
                                <input type="text" id="course_name" name="course_name" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="course_acro">Acronym: <span style="color: #ff0000">*</span></label>
                                <input type="text" id="course_acro" name="course_acro" class="form-control" autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-sm add-dept-btn"><i class="fa fa-plus"></i> Add</button>
                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php";?>
